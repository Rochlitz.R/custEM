﻿.. _authorlabel:

#######
Authors
#######

Primary development, maintanance and corresponding author for **custEM**:

    Raphael Rochlitz - raphael.rochlitz@leibniz-liag.de
    
Current developments, corresponding author regarding iterative solvers:

    Michael Weiss - michael.weiss@leibniz-liag.de
    
Additional support and contact for issues regarding **pyGIMLi**:

    Thomas Günther - thomas.guenther@leibniz-liag.de