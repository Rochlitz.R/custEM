#####################
Maintenance of custEM
#####################

This file will be updated from time to time with more experience in providing
software packages. Currently used as memory aid for releaseing new versions.

################################################################################

**Steps to consider before building new conda version**

1. check version string and release date in 'custEM/VERSION.rst' file 

2. check if changes affect functionality of the *examples* 

3. check if changes affect functionality of the *tutorials*

4. update development.rst

5. check if nomenclature changes require new sphinx docs:

    * change to *custEM/docs* directory
    * update submodules by calling 'sphinx-apidoc --force -o . ../custEM'
	* delete *custEM.tests.rst* and submodule 'tests.rst' in *custEM.rst*
    * build docs loaclly by calling 'make html', if desired

6. 'unset PYTHONPATH' and 'conda deactivate' before building new conda packages!

7. build custem conda package