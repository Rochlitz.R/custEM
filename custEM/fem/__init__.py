# -*- coding: utf-8 -*-
"""
fem
===

Submodules:

- ** frequency_domain_approaches**:
  E-field, H-field, A-V-mixed, A-V-nodal, F-U-mixed
- ** time_domain_approaches**:
  Implicit Euler, Inverse Fourier-Transform based, Rational Arnoldi
- **fem_base** for setting up the finite element kernel
- **fem_utils** for defining approach base class and related utility functions
- **primary_fields** for setting up primary fields
- **assembly** of left-/right-hand-sides using total field approaches

################################################################################
"""

from . fem_base import *
from . fem_utils import *
from . primary_fields import *
from . assembly import *
from . frequency_domain_approaches import *
from . time_domain_approaches import *

# THE END
