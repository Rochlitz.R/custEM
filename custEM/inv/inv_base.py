# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""


import dolfin as df
from dolfin import as_backend_type
import numpy as np
from custEM.misc import logger_print as lp
from custEM.misc import mpi_print as mpp
import os
import gc
import time
import sys
from petsc4py import PETSc
from mpi4py import MPI
import scipy.sparse as sp_sparse
#from custEM.misc import current_mem
from custEM.fem.frequency_domain_approaches import H_vector
from custEM.fem import ModelParameters
from custEM.fem.assembly import SecondaryFieldAssembler
from custEM.fem.assembly import TotalFieldAssembler as Assembler
from custEM.core import Solver as RawSolver
from custEM.inv.jacobian_postproc import MTJacobi


class InversionBase:

    """
    Initialize inversion submodule.
    """

    def __init__(self, PP, IB):

        """
        Initialize arrays and variables.
        """

        self.PP = PP
        self.IB = IB
        self.iteration = 0
        self.skip_domains = [0]
        self.log_scaled = False
        self.dA, self.db, self.Qr, self.Qi = [], [], [], []
        self.inv_ids = []
        self.d_func = df.MeshFunction("size_t", self.PP.FS.mesh,
                                      self.PP.FS.mesh.topology().dim())
        self.tx_ids = [np.arange(self.PP.MP.n_tx, dtype=int) for
                       pi in range(self.PP.MP.n_rx)]

        self.e_fields = False
        self.h_fields = False
        self.tipper = False

        if 'E' in self.PP.MP.approach:
            self.components = [['Ex', 'Ey', 'Hx', 'Hy', 'Hz'] for pi in
                               range(self.PP.MP.n_rx)]
        elif 'H' in self.PP.MP.approach:
            self.components = [['Hx', 'Hy', 'Hz'] for pi in
                               range(self.PP.MP.n_rx)]
        elif 'MT' in self.PP.MP.approach:
            self.components = [['Zxx', 'Zxy', 'Zyx', 'Zyy', 'Tx', 'Ty'] for
                               pi in range(self.PP.MP.n_rx)]

        if df.MPI.rank(self.PP.MP.mpi_cw) == 0:
            if not os.path.isdir(self.PP.export_dir + '_inv'):
                os.mkdir(self.PP.export_dir + '_inv')

    def update_inv_parameters(self, **inv_kwargs):

        """
        Update parameters for jacobian calculation.
        """

        for key in inv_kwargs:
            if key not in self.__dict__:
                lp(self.PP.MP.logger, 50,
                   'Error! Unknown inv parameter set', key)
                lp(self.PP.MP.logger, 50,
                   "...  let's stop before something unexpected happens  ...",
                    pre_dash=False)
                raise SystemExit
        self.__dict__.update(inv_kwargs)

        self.flat_cmp = [item for sublist in self.components for
                         item in sublist]

        for pi in range(self.PP.MP.n_rx):
            if type(self.components[pi]) is not list:
                lp(self.PP.MP.logger, 50,
                   'Error! Jacobian calculation requires a list of components '
                   '\nfor every path, e.g., [["H_x, H_z"], ["H_z"]] for '
                   'two paths. Aborting  ...', pre_dash=False)
                raise SystemExit


    def explicit_jacobian(self, Solver, fi, inv_type, inv_dir, remote_station,
                          solvingapproach,Esys=None,EHh=None,Eoffdiag=None,Ediag=None):                                           

        if inv_type not in ['cells', 'domains']:
            lp(self.PP.MP.logger, 50,
               "Error! the *jacobian* keyword argument for *MOD* class "
               "initialization must be 'cells' (3D) or 'domains' (2.5D), "
               "specifying the calculation type of the sensitivity matrix. "
               "Aborting  ...", pre_dash=False)
            raise SystemExit
        
        self.PP.E_t_r, self.PP.E_t_i = [], []
        for ti in range(self.PP.FE.n_tx):
            E_t_r, E_t_i = self.PP.FS.U[ti].split(True)
            self.PP.E_t_r.append(E_t_r)
            self.PP.E_t_i.append(E_t_i)

        if self.log_scaled:
            print('"log_scale" currently not supported '
                  'anymore. Aborting in inv base ...')
            raise SystemExit

        if any(val in self.flat_cmp for val in
               ['Ex', 'Ey', 'Ez', 'Zxx', 'Zxy', 'Zyx', 'Zyy']):
            self.e_fields = True
        if any(val in self.flat_cmp for val in
               ['Hx', 'Hy', 'Hz', 'Bx', 'By', 'Bz',
                'Zxx', 'Zxy', 'Zyx', 'Zyy', 'Tx', 'Ty']):
            self.h_fields = True
        if any(val in self.flat_cmp for val in ['Tx', 'Ty']):
            self.tipper = True

        self.eval_inv_conditions()
        self.get_mask(inv_dir, fi)

        if inv_type == 'cells':
            if not self.solve_transpose:
                lp(self.PP.MP.logger, 50,
                   'Error! Currently only *solve_transposed* for jacobi '
                   'calculation type *cells* implemented. Aborting  ...',
                    pre_dash=False)
                raise SystemExit

            lp(self.PP.MP.logger, 20,
               '...  calculating explicit jacobian cell-wise '
               ' as G^T A^-1 Q^T  ...', pre_dash=False)
            if '_t' in self.PP.MP.approach or 'MT' in self.PP.MP.approach:
                if solvingapproach=='direct':
                  self.solve_transposed_for_Et_local(Solver, fi, remote_station,
                                                     solvingapproach)
                if solvingapproach=='iterative':  
                  self.solve_transposed_for_Et_local(Solver, fi, remote_station,
                                                     solvingapproach,Esys,EHh, Eoffdiag, Ediag)

            elif '_s' in self.PP.MP.approach:
                self.solve_transposed_for_Es_local(Solver, fi)

        elif inv_type == 'domains':
            self.G = [df.Function(self.PP.FS.M) for tx
                      in range(self.PP.MP.n_tx)]
            for rx_path in self.PP.MP.rx:
                self.IB.Q.append(self.IB.create_interpolation_matrices(
                    np.array(rx_path)))
            if self.solve_transpose:
                lp(self.PP.MP.logger, 20,
                   '...  calculating explicit jacobian domain-wise '
                   ' as G^T A^-1 Q^T  ...', pre_dash=False)

                # TO DO: Go for MatVec or maybe better, stitched local assembly
                if 'E' in self.PP.MP.approach or 'MT' in self.PP.MP.approach:
                    self.solve_transposed_for_E_vecvec(Solver, fi)
                # elif 'H' in self.PP.MP.approach:
                #     self.solve_transposed_magnetic(Solver, fi)
            else:
                lp(self.PP.MP.logger, 20,
                   '...  calculating explicit jacobian as Q A^-1 G  ...',
                   pre_dash=False)
                if 'E' in self.PP.MP.approach or 'MT' in self.PP.MP.approach:
                    print('hacked untransposed to transposed')
                    self.solve_transposed_for_E_vecvec(Solver, fi)
                    # self.solve_untransposed_for_E_approach(Solver, fi)
                # elif 'H' in self.PP.MP.approach:
                #     self.solve_untransposed_magnetic(Solver, fi)

            if self.PP.MP.approach != 'MT':
                if df.MPI.rank(self.PP.MP.mpi_cw) == 0:
                    # for pi in range(self.PP.MP.n_rx):
                    #     np.save(self.PP.export_dir + '_inv/f_' + str(fi) +
                    #             '_J_rx_path_' + str(pi) + '_iter_' +
                    #             str(self.iteration) + '.npy', self.J[pi])

                    np.savez(self.PP.export_dir + '_inv/f_' + str(fi) +
                             '_J.npz', *self.J)
            else:
                Converter = MTJacobi(self.PP, self.J,
                                     np.arange(len(self.inv_ids)), fi,
                                     len(self.inv_ids), self.components,
                                     remote_station)

    def create_Ge_from_db(self, ai, om):

        # trick fenics to avoid n_model pre-compilations
        self.d_func.array()[:] = 0
        self.d_func.array()[
            self.PP.FS.DOM.domain_func.array() == self.inv_ids[ai]] = 1
        dx = df.Measure('dx', subdomain_data=self.d_func)

        # db based on E_t
        for ti in range(self.PP.MP.n_tx):
            self.G[ti].vector()[:] = df.assemble(
                        (om * df.inner(self.PP.E_t_i[ti],
                                       self.PP.FE.v_r) * dx(1) +
                         om * df.inner(self.PP.E_t_r[ti],
                                       self.PP.FE.v_i) * dx(1)))

    def create_Gh_from_db(self, ai):

        siginv = df.Constant((-1. / self.PP.MP.sigma[self.inv_ids[ai]][0]))
        # trick fenics to avoid n_model pre-compilations
        self.d_func.array()[:] = 0
        self.d_func.array()[
            self.PP.FS.DOM.domain_func.array() == self.inv_ids[ai]] = 1
        dx = df.Measure('dx', subdomain_data=self.d_func)

        # db based on E_t
        for ti in range(self.PP.MP.n_tx):
            self.G[ti].vector()[:] = df.assemble((
                          -df.inner(siginv * self.PP.E_t_r[ti],
                                    df.curl(self.PP.FE.v_r)) * dx(1) +
                          df.inner(siginv * self.PP.E_t_i[ti],
                                    df.curl(self.PP.FE.v_i)) * dx(1)))

        # print(self.G[ti].vector()[:][self.G[ti].vector()[:] != 0.])
        # print(np.argwhere(self.G[ti].vector()[:] != 0.))

    def solve_untransposed_for_E_approach(self, E_solver, fi):

        self.J = []
        for pi in range(self.PP.MP.n_rx):
            self.J.append(np.zeros((len(self.inv_ids), len(self.tx_ids[pi]),
                                    len(self.PP.MP.rx[pi]),
                                    len(self.components[pi])), dtype=complex))

        # calc J as Q * A^-1 * G
        JQ = df.Function(self.PP.FS.M)
        ie = [0] * self.PP.MP.n_rx
        om = df.Constant(self.PP.MP.omegas[fi])

        for ai in range(len(self.inv_ids)):
            G_e = self.create_Ge_from_db(ai, om)
            for ti in range(self.PP.MP.n_tx):
                E_solver.solver.solve(JQ.vector(), G_e[ti].vector())
                J_er, J_ei = JQ.split(True)

                for pi in range(self.PP.MP.n_rx):
                    tx_counter = 0
                    if ti in self.tx_ids[pi]:
                        for k in range(len(self.PP.MP.rx[pi])):
                            ie[pi] = 0
                            if 'Ex' in self.components[pi]:
                                self.J[pi][ai, tx_counter, k, ie[pi]] = \
                                    self.IB.Q[pi][k][0].vector().inner(
                                        J_er.vector()) + \
                                    1j * self.IB.Q[pi][k][0].vector().inner(
                                        J_ei.vector())
                                ie[pi] += 1
                            if 'Ey' in self.components[pi]:
                                self.J[pi][ai, tx_counter, k, ie[pi]] = \
                                    self.IB.Q[pi][k][1].vector().inner(
                                        J_er.vector()) + \
                                    1j * self.IB.Q[pi][k][1].vector().inner(
                                        J_ei.vector())
                                ie[pi] += 1
                            if 'Ez' in self.components[pi]:
                                self.J[pi][ai, tx_counter, k, ie[pi]] = \
                                    self.IB.Q[pi][k][2].vector().inner(
                                        J_er.vector()) + \
                                    1j * self.IB.Q[pi][k][2].vector().inner(
                                        J_ei.vector())
                                ie[pi] += 1
                        tx_counter += 1

        # work on H-field related Jacobi entries
        if any(val in self.flat_cmp for val in
               ['Hx', 'Hy', 'Hz', 'Bx', 'By', 'Bz']):

            lp(self.PP.MP.logger, 20,
               '...  factorizing additional system for H-field Jacobian '
               'entries  ...', pre_dash=False)

            # reduce log level to avoid unnecessary prints
            ll = int(self.PP.MP.logger.handlers[0].level)
            if ll < 30:
                self.PP.MP.logger.handlers[0].setLevel(30)

            if not hasattr(self, 'HFE'):
                self.create_magnetic_form()

            # create system for dummy solve with arbitrary rhs
            for ti in range(self.HFE.n_tx):
                if ti == 0:
                    self.A = df.PETScMatrix()
                    df.assemble_system(self.HFE.L[fi], self.HFE.R[ti],
                                       A_tensor=self.A, b_tensor=self.b[ti])
                else:
                    df.assemble(self.HFE.R[ti], tensor=self.b[ti])

            H_solver = RawSolver(self.PP.FS, self.HFE,
                                 mumps_debug=self.PP.MP.mumps_debug,
                                 serial_ordering=self.PP.MP.serial_ordering)
            H_solver.solve_system_mumps(
                      self.A, self.b, self.HFE.FS.M, sym=True)

            # switch back to specified debug level of main model
            self.PP.MP.logger.handlers[0].setLevel(ll)

            for ai in range(len(self.inv_ids)):
                G_h = self.create_Gh_from_db(ai)
                for ti in range(self.PP.MP.n_tx):
                    H_solver.solver.solve(JQ.vector(), G_h[ti].vector())
                    J_hr, J_hi = JQ.split(True)
                    for pi in range(self.PP.MP.n_rx):
                        tx_counter = 0
                        if ti in self.tx_ids[pi]:
                            for k in range(len(self.PP.MP.rx[pi])):
                                ih = 0 + ie[pi]
                                if 'Hx' in self.components[pi] or \
                                   'Bx' in self.components[pi]:
                                    self.J[pi][ai, tx_counter, k, ih] = \
                                        self.IB.Q[pi][k][0].vector().inner(
                                            J_hr.vector()) + \
                                        1j * self.IB.Q[pi][k][0].vector(
                                            ).inner(J_hi.vector())
                                    ih += 1
                                if 'Hy' in self.components[pi] or \
                                   'By' in self.components[pi]:
                                    self.J[pi][ai, tx_counter, k, ih] = \
                                        self.IB.Q[pi][k][1].vector().inner(
                                            J_hr.vector()) + \
                                        1j * self.IB.Q[pi][k][1].vector(
                                            ).inner(J_hi.vector())
                                    ih += 1
                                if 'Hz' in self.components[pi] or \
                                   'Bz' in self.components[pi]:
                                    self.J[pi][ai, tx_counter, k, ih] = \
                                        self.IB.Q[pi][k][2].vector().inner(
                                            J_hr.vector()) + \
                                        1j * self.IB.Q[pi][k][2].vector(
                                            ).inner(J_hi.vector())
                            tx_counter += 1

    def solve_transposed_for_E_vecvec(self, E_solver, fi):

        """
        Solve systems and do Algebra for obtaining full, explicit Jacobian
        entries based on a total E-Field approach for both, E- and H-fields.
        """

        self.J = []
        for pi in range(self.PP.MP.n_rx):
            if self.PP.MP.approach == 'MT':
                # print('TO DO: Adjust Jacobi dim for Z and T')
                if self.e_fields: # MT with Z
                    l_cmp = 4
                else: # VMTF with T
                    l_cmp = 3
            else:
                l_cmp = len(self.components[pi])

            self.J.append(np.zeros((len(self.inv_ids), len(self.tx_ids[pi]),
                                    len(self.PP.MP.rx[pi]),
                                    l_cmp), dtype=complex))

        # calc J as G^T * A^-1 * Q^T
        self.q_func = df.Function(self.PP.FS.M)
        self.q_vals = np.zeros(len(self.q_func.vector()[:]))

        ie = []
        for pi in range(self.PP.MP.n_rx):
            ie.append(0)

        self.eval_mt_components()

        # work on E-field related Jacobi entries
        # calc J as G^T * A^-1 * Q^T
        if any(val in self.flat_cmp for val in
               ['Ex', 'Ey', 'Ez', 'Zxx', 'Zxy', 'Zyx', 'Zyy']):

            om = df.Constant(self.PP.MP.omegas[fi])
            solved = []
            t0 = time.time()
            for pi in range(self.PP.MP.n_rx):
                dummy1 = []
                for ki in range(len(self.PP.MP.rx[pi])):
                    # if df.MPI.rank(df.MPI.comm_world) == 0:
                    #     if not ki % 10:
                    #         print('ki', ki)
                    dummy2 = []
                    if 'Ex' in self.components[pi] or 'Z' in self.mt_cmp[pi]:
                        dummy2.append(self.solve_q_vec(E_solver, pi, ki, 0))

                    if 'Ey' in self.components[pi] or 'Z' in self.mt_cmp[pi]:
                        dummy2.append(self.solve_q_vec(E_solver, pi, ki, 1))

                    if 'Ez' in self.components[pi]:
                        dummy2.append(self.solve_q_vec(E_solver, pi, ki, 2))
                    dummy1.append(dummy2)
                solved.append(dummy1)

            # delete "E"-approach factorization to avoid doubled memory
            # requirements before factorizing "H"-approach system
            del E_solver.solver
            lp(self.PP.MP.logger, 50,
               '  -  time for E-field backsubstitution: ' +
               str(time.time() - t0) + '  -  ', pre_dash=False)

            t0 = time.time()
            for ai in range(len(self.inv_ids)):
                self.create_Ge_from_db(ai, om)
                for pi in range(self.PP.MP.n_rx):
                    for ki in range(len(self.PP.MP.rx[pi])):
                        ie[pi] = 0
                        if 'Ex' in self.components[pi] or \
                           'Z' in self.mt_cmp[pi]:
                            self.fill_jacobi_entry(solved, pi, ki, ie[pi],
                                                   ai)
                            ie[pi] += 1

                        if 'Ey' in self.components[pi] or \
                           'Z' in self.mt_cmp[pi]:
                            self.fill_jacobi_entry(solved, pi, ki, ie[pi],
                                                   ai)
                            ie[pi] += 1

                        if 'Ez' in self.components[pi]:
                            self.fill_jacobi_entry(solved, pi, ki, ie[pi],
                                                   ai)
                            ie[pi] += 1

            lp(self.PP.MP.logger, 50,
                '  -  time for E-field vector algebra: ' +
                str(time.time() - t0) + '  -  ', pre_dash=False)

        # work on H-field related Jacobi entries
        if any(val in self.flat_cmp for val in
               ['Hx', 'Hy', 'Hz', 'Bx', 'By', 'Bz',
                'Zxx', 'Zxy', 'Zyx', 'Zyy', 'Tx', 'Ty']):

            lp(self.PP.MP.logger, 20,
                '...  factorizing additional system for H-field Jacobian '
                'entries  ...', pre_dash=False)

            # reduce log level to avoid unnecessary prints
            ll = int(self.PP.MP.logger.handlers[0].level)
            if ll < 30:
                self.PP.MP.logger.handlers[0].setLevel(30)

            if not hasattr(self, 'HFE'):
                self.create_magnetic_form()

            # create system for dummy solve with arbitrary rhs
            for ti in range(self.HFE.n_tx):
                if ti == 0:
                    self.A = df.PETScMatrix()
                    df.assemble_system(self.HFE.L[fi], self.HFE.R[ti],
                                        A_tensor=self.A, b_tensor=self.b[ti])
                else:
                    df.assemble(self.HFE.R[ti], tensor=self.b[ti])

            H_solver = RawSolver(self.PP.FS, self.HFE,
                                  mumps_debug=self.PP.MP.mumps_debug,
                                  serial_ordering=self.PP.MP.serial_ordering)
            H_solver.solve_system_mumps(
                      self.A, self.b, self.HFE.FS.M, sym=True)

            # switch back to specified debug level of main model
            self.PP.MP.logger.handlers[0].setLevel(ll)

            solved = []
            t0 = time.time()
            for pi in range(self.PP.MP.n_rx):
                dummy1 = []
                for ki in range(len(self.PP.MP.rx[pi])):
                    # if df.MPI.rank(df.MPI.comm_world) == 0:
                    #     if not ki % 10:
                    #         print('ki', ki)
                    dummy2 = []
                    if 'Hx' in self.components[pi] or \
                       'Bx' in self.components[pi] or \
                       self.mt_cmp[pi][0] is not None:
                        dummy2.append(self.solve_q_vec(H_solver, pi, ki, 0))

                    if 'Hy' in self.components[pi] or \
                       'By' in self.components[pi] or \
                       self.mt_cmp[pi][0] is not None:
                        dummy2.append(self.solve_q_vec(H_solver, pi, ki, 1))

                    if 'Hz' in self.components[pi] or \
                       'Bz' in self.components[pi] or \
                       'T' in self.mt_cmp[pi]:
                        dummy2.append(self.solve_q_vec(H_solver, pi, ki, 2))
                    dummy1.append(dummy2)
                solved.append(dummy1)

            del H_solver.solver
            lp(self.PP.MP.logger, 50,
               '  -  time for H-field backsubstitution: ' +
               str(time.time() - t0) + '  -  ', pre_dash=False)

            t0 = time.time()
            for ai in range(len(self.inv_ids)):
                # if df.MPI.rank(df.MPI.comm_world) == 0:
                #     if not ai % 100:
                #         print(ai)
                self.create_Gh_from_db(ai)
                for pi in range(self.PP.MP.n_rx):
                    for ki in range(len(self.PP.MP.rx[pi])):
                        ih = 0 + ie[pi]
                        if 'Hx' in self.components[pi] or \
                           'Bx' in self.components[pi] or \
                           self.mt_cmp[pi][0] is not None:
                            self.fill_jacobi_entry(solved, pi, ki, ih, ai)
                            ih += 1

                        if 'Hy' in self.components[pi] or \
                           'By' in self.components[pi] or \
                           self.mt_cmp[pi][0] is not None:
                            self.fill_jacobi_entry(solved, pi, ki, ih, ai)
                            ih += 1

                        if 'Hz' in self.components[pi] or \
                           'Bz' in self.components[pi] or \
                           'T' in self.mt_cmp[pi]:
                            self.fill_jacobi_entry(solved, pi, ki, ih, ai)

            lp(self.PP.MP.logger, 50,
                '  -  time for H-field vector algebra: ' +
                str(time.time() - t0) + '  -  ', pre_dash=False)

    def solve_transposed_for_E_matvec(self, E_solver, fi):

        """
        Solve systems and do Algebra for obtaining full, explicit Jacobian
        entries based on a total E-Field approach for both, E- and H-fields.
        """

        if any(len(tx) > 1 for tx in self.tx_ids):
            lp(self.PP.MP.logger, 50,
               "Error! *MatVec* product implementation for filling Jacobi "
               "entries does not allow multiple Tx for single receiver paths."
               " Aborting  ...",
                pre_dash=False)
            raise SystemExit

        # calc J as G^T * A^-1 * Q^T
        self.J = []
        self.q_func = df.Function(self.PP.FS.M)
        self.q_vals = np.zeros(len(self.q_func.vector()[:]))
        self.JG = df.Function(self.PP.FS.M)
        self.tmp_vals = np.empty(len(self.JG.vector()[:]))
        self.prepare_jacobi_fill()

        ie = []
        for pi in range(self.PP.MP.n_rx):
            ie.append(0)

        # work on E-field related Jacobi entries
        # calc J as G^T * A^-1 * Q^T
        if 'Ex' in self.flat_cmp or 'Ey' in self.flat_cmp or \
           'Ez' in self.flat_cmp:

            om = df.Constant(self.PP.MP.omegas[fi])
            t0 = time.time()
            for pi in range(self.PP.MP.n_rx):
                offset = 0
                for ki in range(len(self.PP.MP.rx[pi])):
                    if 'Ex' in self.components[pi]:
                        self.solve_q_mat(E_solver, pi, ki, 0, 'E', offset)
                        offset += 1
                #for ki in range(len(self.PP.MP.rx[pi])):
                    if 'Ey' in self.components[pi]:
                        self.solve_q_mat(E_solver, pi, ki, 1, 'E', offset)
                        offset += 1
                #for ki in range(len(self.PP.MP.rx[pi])):
                    if 'Ez' in self.components[pi]:
                        self.solve_q_mat(E_solver, pi, ki, 2, 'E', offset)
                        offset += 1

            # delete "E"-approach factorization to avoid doubled memory
            # requirements before factorizing "H"-approach system
            del E_solver.solver
            self.x = []
            for pi in range(self.PP.MP.n_rx):
                if self.AQe_real[pi] is not None:
                    self.AQe_real[pi].assemble()
                    self.AQe_imag[pi].assemble()
                    x, dummy = self.AQe_real[pi].getVecs()
                    self.x.append(x)
                else:
                    self.x.append([])

            lp(self.PP.MP.logger, 50,
               '  -  time for E-field backsubstitution: ' +
               str(time.time() - t0) + '  -  ', pre_dash=False)

            t0 = time.time()
            for ai in range(len(self.inv_ids)):
                self.create_Ge_from_db(ai, om)
                self.fill_jacobi_row_e(ai)

            lp(self.PP.MP.logger, 50,
                '  -  time for E-field matrix algebra: ' +
                str(time.time() - t0) + '  -  ', pre_dash=False)

        # work on H-field related Jacobi entries
        if any(val in self.flat_cmp for val in
               ['Hx', 'Hy', 'Hz', 'Bx', 'By', 'Bz']):

            lp(self.PP.MP.logger, 20,
                '...  factorizing additional system for H-field Jacobian '
                'entries  ...', pre_dash=False)

            # reduce log level to avoid unnecessary prints
            ll = int(self.PP.MP.logger.handlers[0].level)
            if ll < 30:
                self.PP.MP.logger.handlers[0].setLevel(30)

            if not hasattr(self, 'HFE'):
                self.create_magnetic_form()

            # create system for dummy solve with arbitrary rhs
            for ti in range(self.HFE.n_tx):
                if ti == 0:
                    self.A = df.PETScMatrix()
                    df.assemble_system(self.HFE.L[fi], self.HFE.R[ti],
                                        A_tensor=self.A, b_tensor=self.b[ti])
                else:
                    df.assemble(self.HFE.R[ti], tensor=self.b[ti])

            H_solver = RawSolver(self.PP.FS, self.HFE,
                                  mumps_debug=self.PP.MP.mumps_debug,
                                  serial_ordering=self.PP.MP.serial_ordering)
            H_solver.solve_system_mumps(
                      self.A, self.b, self.HFE.FS.M, sym=True)

            # switch back to specified debug level of main model
            self.PP.MP.logger.handlers[0].setLevel(ll)

            t0 = time.time()
            for pi in range(self.PP.MP.n_rx):
                offset = 0
                for ki in range(len(self.PP.MP.rx[pi])):
                    if 'Hx' in self.components[pi] or \
                       'Bx' in self.components[pi]:
                        self.solve_q_mat(H_solver, pi, ki, 0, 'H', offset)
                        offset += 1
                for ki in range(len(self.PP.MP.rx[pi])):
                    if 'Hy' in self.components[pi] or \
                       'By' in self.components[pi]:
                        self.solve_q_mat(H_solver, pi, ki, 1, 'H', offset)
                        offset += 1
                for ki in range(len(self.PP.MP.rx[pi])):
                    if 'Hz' in self.components[pi] or \
                       'Bz' in self.components[pi]:
                        self.solve_q_mat(H_solver, pi, ki, 2, 'H', offset)
                        offset += 1

            del H_solver.solver
            self.x = []
            for pi in range(self.PP.MP.n_rx):
                if self.AQh_real[pi] is not None:
                    self.AQh_real[pi].assemble()
                    self.AQh_imag[pi].assemble()
                    x, dummy = self.AQh_real[pi].getVecs()
                    self.x.append(x)
                else:
                    self.x.append([])

            lp(self.PP.MP.logger, 50,
               '  -  time for H-field backsubstitution: ' +
               str(time.time() - t0) + '  -  ', pre_dash=False)


            t0 = time.time()
            for ai in range(len(self.inv_ids)):
                self.create_Gh_from_db(ai)
                self.fill_jacobi_row_h(ai)

            lp(self.PP.MP.logger, 50,
                '  -  time for H-field matrix algebra: ' +
                str(time.time() - t0) + '  -  ', pre_dash=False)

            # reshape output
            if df.MPI.rank(df.MPI.comm_world) == 0:
                for pi in range(self.PP.MP.n_rx):
                    self.J[pi] = self.J[pi].reshape(len(self.inv_ids),
                                                    1,
                                                    len(self.PP.MP.rx[pi]),
                                                    len(self.components[pi]))


    def solve_transposed_for_Et_local(self, E_solver, fi, remote_station,
                                      solvingapproach,Esys=None,EHh=None,Eoffdiag=None,Ediag=None):


        """
        Solve systems and do Algebra for obtaining full, explicit Jacobian
        entries based on a total E-Field approach for both, E- and H-fields.
        """

        # calc J as G^T * A^-1 * Q^T
        self.q_func = df.Function(self.PP.FS.M)
        self.q_vals = np.zeros(len(self.q_func.vector()[:]))
        self.JGr = df.Function(self.PP.FS.M)
        if df.MPI.rank(df.MPI.comm_world) == 0:
            self.imag_vals = np.empty(self.JGr.vector().size())
        else:
            self.imag_vals = None

        t0 = time.time()
        all_cells = [cell for cell in df.cells(self.PP.FS.mesh)]
        allc = self.PP.MP.mpi_cw.gather(len(all_cells), root=0)
        max_cells = self.PP.MP.mpi_cw.bcast(np.max(allc), root=0)

        while len(all_cells) < max_cells:
            all_cells.append(all_cells[0])
        df.MPI.barrier(df.MPI.comm_world)

        if self.e_fields:
            om = df.Constant(self.PP.MP.omegas[fi])
            formsE = []
            for ti in range(self.PP.MP.n_tx):
                formsE.append(om * df.inner(self.PP.E_t_i[ti],
                                            self.PP.FE.v_r) * df.dx +
                              om * df.inner(self.PP.E_t_r[ti],
                                            self.PP.FE.v_i) * df.dx)

        if self.h_fields:
            formsH = []
            for ti in range(self.PP.MP.n_tx):
                formsH.append(-df.inner(self.PP.E_t_r[ti],
                                        df.curl(self.PP.FE.v_r)) * df.dx +
                              df.inner(self.PP.E_t_i[ti],
                                       df.curl(self.PP.FE.v_i)) * df.dx)

        if self.PP.FS.p == 1:
            width = 12
        elif self.PP.FS.p == 2:
            width = 40

        ids = []
        dofmap = self.PP.FS.M.dofmap()

        for ci in range(max_cells):
            idx = dofmap.cell_dofs(all_cells[ci].index())
            ids.append(idx)

        if self.e_fields:
            GE = np.zeros((self.PP.MP.n_tx, max_cells, width))
            for ci in range(max_cells):
                for ti in range(self.PP.MP.n_tx):
                    GE[ti, ci, :] = df.assemble_local(formsE[ti],
                                                      all_cells[ci])
            GE = GE[:, :len(all_cells), :]

        if self.h_fields:
            GH = np.zeros((self.PP.MP.n_tx, max_cells, width))
            for ci in range(max_cells):
                for ti in range(self.PP.MP.n_tx):
                    GH[ti, ci, :] = df.assemble_local(formsH[ti],
                                                      all_cells[ci])
            GH = GH[:, :len(all_cells), :]

        ids = np.array(ids)[:len(all_cells)]
        cids = []
        bcids = []

        for ai, am in enumerate(self.PP.FS.DOM.domain_func.array()):
            tmp = np.argwhere(am == self.inv_ids)
            if len(tmp) > 0:
                cids.append(ai)
                bcids.append(tmp[0][0])
                if self.h_fields:
                    GH[:, ai, :] /= -self.PP.MP.sigma[
                        self.inv_ids[tmp[0][0]]][0]

        if self.e_fields:
            self.GE = GE[:, cids, :]

        if self.h_fields:
            self.GH = GH[:, cids, :]

        ids = ids[cids]
        length = len(ids)
        self.gids = np.zeros(ids.shape, dtype=int)
        for i in range(ids.shape[0]):
            for j in range(ids.shape[1]):
                self.gids[i, j] = dofmap.local_to_global_index(ids[i, j])

        lp(self.PP.MP.logger, 20,
           '  -  time for local assembly: ' +
           str(time.time() - t0) + '  -  ', pre_dash=False)

        self.J_local = []
        for pi in range(self.PP.MP.n_rx):
            l_cmp = self.get_cmp_size(pi)
            self.J_local.append(
                np.zeros((length, len(self.tx_ids[pi]), len(self.PP.MP.rx[pi]),
                          l_cmp), dtype=complex))

        self.eval_mt_components()

        # work on E-field related Jacobi entries
        t0 = time.time()
        ie = [0] * self.PP.MP.n_rx
        solvecounter=0
        
        if solvingapproach=='direct':
        
          for pi in range(self.PP.MP.n_rx):
              for ki in range(len(self.PP.MP.rx[pi])):
                  ie[pi] = 0
                  if self.e_fields:
                      qvec = self.IB.create_interpolation_matrices(
                          np.array([self.PP.MP.rx[pi][ki]]))
                  if 'Ex' in self.components[pi] or 'Z' in self.mt_cmp[pi]:
                      if np.any(self.mask[pi][:, ki, ie[pi]]):
                          self.solve_q_on_the_fly(E_solver, pi, ki, qvec[0][0])
                          solvecounter += 1
                          self.scatter()
                          self.multiply(pi, ki, ie[pi])
                      ie[pi] += 1

                  if 'Ey' in self.components[pi] or 'Z' in self.mt_cmp[pi]:
                      if np.any(self.mask[pi][:, ki, ie[pi]]):
                          self.solve_q_on_the_fly(E_solver, pi, ki, qvec[0][1])
                          solvecounter += 1
                          self.scatter()
                          self.multiply(pi, ki, ie[pi])
                      ie[pi] += 1

                  if 'Ez' in self.components[pi]:
                      if np.any(self.mask[pi][:, ki, ie[pi]]):
                          self.solve_q_on_the_fly(E_solver, pi, ki, qvec[0][2])
                          solvecounter += 1
                          self.scatter()
                          self.multiply(pi, ki, ie[pi])
                      ie[pi] += 1

        if solvingapproach=='iterative':
        
          ksp,f1,f2,g,h,Ag,H,A11,Asystem,Aw,w,r,DD,AD=E_solver.build_PRESB_Jacobian(Esys, EHh, Eoffdiag, Ediag,
                                                                                    inner_tol=1e-1)
          
          for pi in range(self.PP.MP.n_rx):
              for ki in range(len(self.PP.MP.rx[pi])):
                  ie[pi] = 0
                  if self.e_fields:
                      qvec = self.IB.create_interpolation_matrices(
                          np.array([self.PP.MP.rx[pi][ki]]))
                  if 'Ex' in self.components[pi] or 'Z' in self.mt_cmp[pi]:
                      if np.any(self.mask[pi][:, ki, ie[pi]]):
                          self.q_vals[::2] = qvec[0][0].vector()[:]
                          self.q_func.vector()[:] = self.q_vals
                          self.JGr=E_solver.solve_PRESB_Jacobian(ksp,f1,f2,g,h,Ag,H,A11,Asystem,Aw,w,r,DD,AD,
                                                                 self.q_func.vector(),fs=self.PP.FS.M, flag_mt=False,
                                                                 outer_tol=1e-4)
                          solvecounter += 1
                          self.scatter()
                          self.multiply(pi, ki, ie[pi])
                      ie[pi] += 1

                  if 'Ey' in self.components[pi] or 'Z' in self.mt_cmp[pi]:
                      if np.any(self.mask[pi][:, ki, ie[pi]]):
                          self.q_vals[::2] = qvec[0][1].vector()[:]
                          self.q_func.vector()[:] = self.q_vals
                          self.JGr=E_solver.solve_PRESB_Jacobian(ksp,f1,f2,g,h,Ag,H,A11,Asystem,Aw,w,r,DD,AD,
                                                                 self.q_func.vector(),fs=self.PP.FS.M, flag_mt=False,
                                                                 outer_tol=1e-4)
                          solvecounter += 1
                          self.scatter()
                          self.multiply(pi, ki, ie[pi])
                      ie[pi] += 1

                  if 'Ez' in self.components[pi]:
                      if np.any(self.mask[pi][:, ki, ie[pi]]):
                          self.q_vals[::2] = qvec[0][2].vector()[:]
                          self.q_func.vector()[:] = self.q_vals
                          self.JGr=E_solver.solve_PRESB_Jacobian(ksp,f1,f2,g,h,Ag,H,A11,Asystem,Aw,w,r,DD,AD,
                                                                 self.q_func.vector(),fs=self.PP.FS.M, flag_mt=False,
                                                                 outer_tol=1e-4)
                          solvecounter += 1
                          self.scatter()
                          self.multiply(pi, ki, ie[pi])
                      ie[pi] += 1

        
        if df.MPI.rank(df.MPI.comm_world) == 0:
          print("Number of E-field related solves: ", solvecounter)
        # current_mem(fmt='MiB')
        if solvingapproach == 'direct':
          del E_solver.solver
        lp(self.PP.MP.logger, 20,
           '  -  time for E-field backsubstitution and local algebra: ' +
           str(time.time() - t0) + '  -  ', pre_dash=False)
        # current_mem(total=True, fmt='MiB')

        # work on H-field related Jacobi entries
        t0 = time.time()

        if self.h_fields:
            if solvingapproach == 'direct':
              H_solver = self.magnetic_factorization(fi,solvingapproach)
            if solvingapproach == 'iterative':
              H_solver, Asys, Hh, Aoffdiag, Adiag = self.magnetic_factorization(fi,solvingapproach)
            
        # current_mem(fmt='MiB')
        lp(self.PP.MP.logger, 20,
           '  -  time for H-field factorization: ' +
           str(time.time() - t0) + '  -  ', pre_dash=False)

        t0 = time.time()
        t11 = time.time()
        
        if solvingapproach == 'direct':
          
          for pi in range(self.PP.MP.n_rx):
              for ki in range(len(self.PP.MP.rx[pi])):
                  ih = ie[pi]
                  if self.h_fields:
                      qvec = self.IB.create_interpolation_matrices(
                          np.array([self.PP.MP.rx[pi][ki]]))
                  if 'Hx' in self.components[pi] or \
                     'Bx' in self.components[pi] or \
                     self.mt_cmp[pi][0] is not None:
                    if np.any(self.mask[pi][:, ki, ih]):
                      self.solve_q_on_the_fly(H_solver, pi, ki, qvec[0][0])
                      solvecounter += 1                                 
                      self.scatter()
                      self.multiply(pi, ki, ih, 'H')
                    ih += 1
  
                  if 'Hy' in self.components[pi] or \
                     'By' in self.components[pi] or \
                     self.mt_cmp[pi][0] is not None:
                      if np.any(self.mask[pi][:, ki, ih]):
                        self.solve_q_on_the_fly(H_solver, pi, ki, qvec[0][1])
                        solvecounter += 1                                     
                        self.scatter()
                        self.multiply(pi, ki, ih, 'H')
                      ih += 1
  
                  if 'Hz' in self.components[pi] or \
                     'Bz' in self.components[pi] or \
                     'T' in self.mt_cmp[pi]:
                      if np.any(self.mask[pi][:, ki, ih]):
                        self.solve_q_on_the_fly(H_solver, pi, ki, qvec[0][2])
                        solvecounter += 1
                        self.scatter()
                        self.multiply(pi, ki, ih, 'H')
  
  
                  if df.MPI.rank(df.MPI.comm_world) == 0:
                    if not ki % 100:
                      print('solved 100 Obs', ki, time.time() - t11)
                      t11 = time.time()
        
        if solvingapproach == 'iterative':
          
          ksp,f1,f2,g,h,Ag,H,A11,Asystem,Aw,w,r,DD,AD=H_solver.build_PRESB_Jacobian(self.Asys, self.Hh, self.Aoffdiag, self.Adiag,
                                                                                    inner_tol=1e-1)
        
          if df.MPI.comm_world.rank==0:
            for pi in range(self.PP.MP.n_rx):
              print("lenth(",pi,")=", len(self.PP.MP.rx[pi]))
          
          for pi in range(self.PP.MP.n_rx):
              for ki in range(len(self.PP.MP.rx[pi])):
                  ih = ie[pi]
                  if self.h_fields:
                  #if self.e_fields:
                      qvec = self.IB.create_interpolation_matrices(
                          np.array([self.PP.MP.rx[pi][ki]]))
                  if 'Hx' in self.components[pi] or \
                     'Bx' in self.components[pi] or \
                     self.mt_cmp[pi][0] is not None:
                      if np.any(self.mask[pi][:, ki, ih]):
                        self.q_vals[::2] = qvec[0][0].vector()[:]
                        self.q_func.vector()[:] = self.q_vals
                        self.JGr=H_solver.solve_PRESB_Jacobian(ksp,f1,f2,g,h,Ag,H,A11,Asystem,Aw,w,r,DD,AD,
                                                               self.q_func.vector(),fs=self.PP.FS.M, flag_mt=False,
                                                               outer_tol=1e-4)
                        solvecounter += 1
                        self.scatter()
                        self.multiply(pi, ki, ih, 'H')
                      ih += 1
  
                  if 'Hy' in self.components[pi] or \
                     'By' in self.components[pi] or \
                     self.mt_cmp[pi][0] is not None:
                      if np.any(self.mask[pi][:, ki, ih]):
                        self.q_vals[::2] = qvec[0][1].vector()[:]
                        self.q_func.vector()[:] = self.q_vals
                        self.JGr=H_solver.solve_PRESB_Jacobian(ksp,f1,f2,g,h,Ag,H,A11,Asystem,Aw,w,r,DD,AD,
                                                               self.q_func.vector(),fs=self.PP.FS.M, flag_mt=False,
                                                               outer_tol=1e-4)
                        
                        solvecounter += 1
                        self.scatter()
                        self.multiply(pi, ki, ih, 'H')
                      ih += 1
                      
                  if 'Hz' in self.components[pi] or \
                     'Bz' in self.components[pi] or \
                     'T' in self.mt_cmp[pi]:
                      if np.any(self.mask[pi][:, ki, ih]):
                        self.q_vals[::2] = qvec[0][2].vector()[:]
                        self.q_func.vector()[:] = self.q_vals
                        self.JGr=H_solver.solve_PRESB_Jacobian(ksp,f1,f2,g,h,Ag,H,A11,Asystem,Aw,w,r,DD,AD,
                                                               self.q_func.vector(),fs=self.PP.FS.M, flag_mt=False,
                                                               outer_tol=1e-4)
                        
                        solvecounter += 1
                        self.scatter()
                        self.multiply(pi, ki, ih, 'H')
  
                  if df.MPI.rank(df.MPI.comm_world) == 0:
                    if not ki % 100:
                      print('solved 100 Obs', ki, time.time() - t11)
                      t11 = time.time()
 
        if df.MPI.rank(df.MPI.comm_world) == 0:
          print("Number of E- and H-field related solves: ", solvecounter)
        
        if self.h_fields:
          if solvingapproach == 'direct':
            del H_solver.solver
        #current_mem(fmt='MiB')
        lp(self.PP.MP.logger, 20,
           '  -  time for H-field backsubstitution and local algebra: ' +
           str(time.time() - t0) + '  -  ', pre_dash=False)

        if self.PP.MP.approach != 'MT':
            np.savez(self.PP.export_dir + '_inv/f_' + str(fi) +
                     '_J_proc_' + str(df.MPI.rank(df.MPI.comm_world)) +
                     '.npz', *self.J_local, bcids)
        else:
            Converter = MTJacobi(self.PP, self.J_local, bcids, fi,
                                 len(self.inv_ids), self.components,
                                     remote_station)


        # J_all = df.MPI.comm_world.gather(J_local, root=0)
        # bcids_all = df.MPI.comm_world.gather(bcids, root=0)

        # if df.MPI.rank(df.MPI.comm_world) == 0:
        #     for pi in range(self.PP.MP.n_rx):
        #         for j in range(len(J_all)):
        #             self.J[pi][bcids_all[j], :, :, :] = J_all[j][pi]

    def solve_transposed_for_Es_local(self, E_solver, fi):

        """
        Solve systems and do Algebra for obtaining full, explicit Jacobian
        entries based on a total E-Field approach for both, E- and H-fields.
        """

        # calc J as G^T * A^-1 * Q^T
        self.q_func = df.Function(self.PP.FS.M)
        self.q_vals = np.zeros(len(self.q_func.vector()[:]))
        self.JGr = df.Function(self.PP.FS.M)
        if df.MPI.rank(df.MPI.comm_world) == 0:
            self.imag_vals = np.empty(self.JGr.vector().size())
        else:
            self.imag_vals = None

        t0 = time.time()
        all_cells = [cell for cell in df.cells(self.PP.FS.mesh)]
        allc = self.PP.MP.mpi_cw.gather(len(all_cells), root=0)
        max_cells = self.PP.MP.mpi_cw.bcast(np.max(allc), root=0)

        while len(all_cells) < max_cells:
            all_cells.append(all_cells[0])
        df.MPI.barrier(df.MPI.comm_world)

        if self.e_fields:
            om = df.Constant(self.PP.MP.omegas[fi])
            formsE = []
            for ti in range(self.PP.MP.n_tx):
                formsE.append(om * df.inner(self.PP.E_t_i[ti],
                                            self.PP.FE.v_r) * df.dx +
                              om * df.inner(self.PP.E_t_r[ti],
                                            self.PP.FE.v_i) * df.dx)

            Ae_form = df.inner(self.PP.FE.u_i,
                               self.PP.FE.v_r) * self.PP.FS.DOM.dx(di) + \
                      df.inner(self.PP.FE.u_r,
                               self.PP.FE.v_i) * self.PP.FS.DOM.dx(di)

        if self.h_fields:
            formsH = []
            for ti in range(self.PP.MP.n_tx):
                formsH.append(-df.inner(self.PP.E_t_r[ti],
                                        df.curl(self.PP.FE.v_r)) * df.dx +
                              df.inner(self.PP.E_t_i[ti],
                                       df.curl(self.PP.FE.v_i)) * df.dx)

            # Ah_form = df.inner(self.PP.FE.u_i,
            #                    self.PP.FE.v_r) * self.PP.FS.DOM.dx(di) + \
            #           df.inner(self.PP.FE.u_r,
            #                    self.PP.FE.v_i) * self.PP.FS.DOM.dx(di)

        if self.PP.FS.p == 1:
            width = 12
        elif self.PP.FS.p == 2:
            width = 40

        ids = []
        dofmap = self.PP.FS.M.dofmap()

        for ci in range(max_cells):
            idx = dofmap.cell_dofs(all_cells[ci].index())
            ids.append(idx)

        if self.e_fields:
            GE = np.zeros((self.PP.MP.n_tx, max_cells, width))
            for ci in range(max_cells):
                for ti in range(self.PP.MP.n_tx):
                    GE[ti, ci, :] = df.assemble_local(formsE[ti],
                                                      all_cells[ci])
            GE = GE[:, :len(all_cells), :]

        if self.h_fields:
            GH = np.zeros((self.PP.MP.n_tx, max_cells, width))
            for ci in range(max_cells):
                for ti in range(self.PP.MP.n_tx):
                    GH[ti, ci, :] = df.assemble_local(formsH[ti],
                                                      all_cells[ci])
            GH = GH[:, :len(all_cells), :]

        ids = np.array(ids)[:len(all_cells)]
        cids = []
        bcids = []

        for ai, am in enumerate(self.PP.FS.DOM.domain_func.array()):
            tmp = np.argwhere(am == self.inv_ids)
            if len(tmp) > 0:
                cids.append(ai)
                bcids.append(tmp[0][0])
                if self.h_fields:
                    GH[:, ai, :] /= -self.PP.MP.sigma[
                        self.inv_ids[tmp[0][0]]][0]

        if self.e_fields:
            self.GE = GE[:, cids, :]

        if self.h_fields:
            self.GH = GH[:, cids, :]

        ids = ids[cids]
        length = len(ids)
        self.gids = np.zeros(ids.shape, dtype=int)
        for i in range(ids.shape[0]):
            for j in range(ids.shape[1]):
                self.gids[i, j] = dofmap.local_to_global_index(ids[i, j])

        if df.MPI.rank(df.MPI.comm_world) == 0:
            self.J = []
            for pi in range(self.PP.MP.n_rx):
                self.J.append(np.zeros((len(self.inv_ids),
                                        len(self.tx_ids[pi]),
                                        len(self.PP.MP.rx[pi]),
                                        len(self.components[pi])),
                                       dtype=complex))

        lp(self.PP.MP.logger, 50,
           '  -  time for local assembly: ' +
           str(time.time() - t0) + '  -  ', pre_dash=False)

        self.J_local = []
        for pi in range(self.PP.MP.n_rx):
            self.J_local.append(
                np.zeros((length, len(self.tx_ids[pi]), len(self.PP.MP.rx[pi]),
                          len(self.components[pi])), dtype=complex))

        # work on E-field related Jacobi entries
        t0 = time.time()
        ie = [0] * self.PP.MP.n_rx
        for pi in range(self.PP.MP.n_rx):
            for ki in range(len(self.PP.MP.rx[pi])):
                ie[pi] = 0
                if self.e_fields:
                    qvec = self.IB.create_interpolation_matrices(
                        np.array([self.PP.MP.rx[pi][ki]]))
                if 'Ex' in self.components[pi]:
                    self.solve_q_on_the_fly(E_solver, pi, ki, qvec[0][0])
                    self.scatter()
                    self.multiply(pi, ki, ie[pi])
                    ie[pi] += 1

                if 'Ey' in self.components[pi]:
                    self.solve_q_on_the_fly(E_solver, pi, ki, qvec[0][1])
                    self.scatter()
                    self.multiply(pi, ki, ie[pi])
                    ie[pi] += 1

                if 'Ez' in self.components[pi]:
                    self.solve_q_on_the_fly(E_solver, pi, ki, qvec[0][2])
                    self.scatter()
                    self.multiply(pi, ki, ie[pi])
                    ie[pi] += 1

        #current_mem(fmt='MiB')
        del E_solver.solver
        lp(self.PP.MP.logger, 50,
           '  -  time for E-field backsubstitution and local algebra: ' +
           str(time.time() - t0) + '  -  ', pre_dash=False)
        #current_mem(total=True, fmt='MiB')

        # work on H-field related Jacobi entries
        t0 = time.time()

        if self.h_fields:
            H_solver = self.magnetic_factorization(fi)
        #current_mem(fmt='MiB')

        lp(self.PP.MP.logger, 50,
           '  -  time for H-field factorization: ' +
           str(time.time() - t0) + '  -  ', pre_dash=False)

        t0 = time.time()
        t11 = time.time()
        for pi in range(self.PP.MP.n_rx):
            for ki in range(len(self.PP.MP.rx[pi])):
                ih = ie[pi]
                if self.h_fields:
                    qvec = self.IB.create_interpolation_matrices(
                        np.array([self.PP.MP.rx[pi][ki]]))
                if 'Hx' in self.components[pi] or \
                    'Bx' in self.components[pi]:
                    self.solve_q_on_the_fly(H_solver, pi, ki, qvec[0][0])
                    self.scatter()
                    self.multiply(pi, ki, ih, 'H')
                    ih += 1

                if 'Hy' in self.components[pi] or \
                    'By' in self.components[pi]:
                    self.solve_q_on_the_fly(H_solver, pi, ki, qvec[0][1])
                    self.scatter()
                    self.multiply(pi, ki, ih, 'H')
                    ih += 1

                if 'Hz' in self.components[pi] or \
                    'Bz' in self.components[pi]:
                    self.solve_q_on_the_fly(H_solver, pi, ki, qvec[0][2])
                    self.scatter()
                    self.multiply(pi, ki, ih, 'H')

                # # for debugging
                # if df.MPI.rank(df.MPI.comm_world) == 0:
                #     if not ki % 100:
                #         print('solved 100 Obs', ki, time.time() - t11)
                #         t11 = time.time()

        del H_solver.solver
        #current_mem(fmt='MiB')
        lp(self.PP.MP.logger, 50,
           '  -  time for H-field backsubstitution and local algebra: ' +
           str(time.time() - t0) + '  -  ', pre_dash=False)

        np.savez(self.PP.export_dir + '_inv/f_' + str(fi) +
                 '_J_proc_' + str(df.MPI.rank(df.MPI.comm_world)) +
                 '.npz', *self.J_local, bcids)

        # J_all = df.MPI.comm_world.gather(J_local, root=0)
        # bcids_all = df.MPI.comm_world.gather(bcids, root=0)

        # if df.MPI.rank(df.MPI.comm_world) == 0:
        #     for pi in range(self.PP.MP.n_rx):
        #         for j in range(len(J_all)):
        #             self.J[pi][bcids_all[j], :, :, :] = J_all[j][pi]

    def solve_q_on_the_fly(self, Solver, pi, ki, q):

        self.q_vals[::2] = q.vector()[:]
        self.q_func.vector()[:] = self.q_vals
        
        Solver.solver.solve(self.JGr.vector(), self.q_func.vector())
         
        # if df.MPI.rank(df.MPI.comm_world) == 0:
        #     print('backsubst', time.time() - t1)
        

    def solve_q_vec(self, Solver, pi, ki, cmp):

        JGr, JGi = df.Function(self.PP.FS.M), df.Function(self.PP.FS.M)
        self.q_vals[::2] = self.IB.Q[pi][ki][cmp].vector()[:]
        self.q_func.vector()[:] = self.q_vals
        Solver.solver.solve(JGr.vector(), self.q_func.vector())

        # avoid solving 2nd time for imag
        self.tmp_vals = np.empty(len(JGr.vector()[:]))
        self.tmp_vals[1::2] = -JGr.vector()[::2]
        self.tmp_vals[::2] = JGr.vector()[1::2]
        JGi.vector()[:] = self.tmp_vals

        return([JGr.vector(), JGi.vector()])

    def solve_q_mat(self, Solver, pi, ki, cmp, EH, offset):

        self.q_vals[::2] = self.IB.Q[pi][ki][cmp].vector()[:]
        self.q_func.vector()[:] = self.q_vals
        Solver.solver.solve(self.JG.vector(), self.q_func.vector())

        # avoid solving 2nd time for imag
        self.tmp_vals[1::2] = -self.JG.vector()[::2]
        self.tmp_vals[::2] = self.JG.vector()[1::2]

        # fill matrix column
        if EH == 'E':
            self.AQe_real[pi].setValuesLocal(self.fill_ids, offset,
                                             self.JG.vector()[:])
            self.AQe_imag[pi].setValuesLocal(self.fill_ids, offset,
                                             self.tmp_vals)
        if EH == 'H':
            self.AQh_real[pi].setValuesLocal(self.fill_ids, offset,
                                             self.JG.vector()[:])
            self.AQh_imag[pi].setValuesLocal(self.fill_ids, offset,
                                             self.tmp_vals)

    def fill_jacobi_entry(self, solved, pi, ki, ii, ai):

        for ti, tid in enumerate(self.tx_ids[pi]):
            self.J[pi][ai, ti, ki, ii] = \
                self.G[tid].vector().inner(solved[pi][ki][ii][0]) + \
                1j * self.G[tid].vector().inner(solved[pi][ki][ii][1])

    def fill_jacobi_row_e(self, ai):

        for pi in range(self.PP.MP.n_rx):
            if self.AQe_real[pi] is not None:
                self.AQe_real[pi].multTranspose(
                    self.G[self.tx_ids[pi][0]].vector().vec(), self.x[pi])
                if df.MPI.rank(df.MPI.comm_world) == 0:
                    self.J[pi][ai, :].real = self.x[pi].array
                self.AQe_imag[pi].multTranspose(
                    self.G[self.tx_ids[pi][0]].vector().vec(), self.x[pi])
                if df.MPI.rank(df.MPI.comm_world) == 0:
                    self.J[pi][ai, :].imag = self.x[pi].array

    def fill_jacobi_row_h(self, ai):

        for pi in range(self.PP.MP.n_rx):
            if self.AQh_real[pi] is not None:
                self.AQh_real[pi].multTranspose(
                    self.G[self.tx_ids[pi][0]].vector().vec(), self.x[pi])
                if df.MPI.rank(df.MPI.comm_world) == 0:
                    self.J[pi][ai, :].real = self.x[pi].array
                self.AQh_imag[pi].multTranspose(
                    self.G[self.tx_ids[pi][0]].vector().vec(), self.x[pi])
                if df.MPI.rank(df.MPI.comm_world) == 0:
                    self.J[pi][ai, :].imag = self.x[pi].array

    def scatter(self):

        # option 1
        # faster option but with memory leaks, no clue how to avoid this bug
        # self.vecr = self.JGr.vector().gather_on_zero()
        # self.vecr = df.MPI.comm_world.bcast(self.vecr, root=0)
        # self.veci = self.JGi.vector().gather_on_zero()
        # self.veci = df.MPI.comm_world.bcast(self.veci, root=0)

        # option 2
        # slower option but not hundreds of GB memory leaks!

        if df.MPI.rank(df.MPI.comm_world) != 0:
            df.MPI.comm_world.send(self.JGr.vector().get_local(),
                                    dest=0, tag=99)
            data = None
        if df.MPI.rank(df.MPI.comm_world) == 0:
            data = self.JGr.vector().get_local()
            for j in range(1, df.MPI.size(df.MPI.comm_world)):
                data = np.append(data,
                                 df.MPI.comm_world.recv(source=j, tag=99))
            # avoid solving 2nd time for imag
            self.imag_vals[1::2] = -data[::2]
            self.imag_vals[::2] = data[1::2]

        self.vecr = df.MPI.comm_world.bcast(data, root=0)
        self.veci = df.MPI.comm_world.bcast(self.imag_vals, root=0)

    def multiply(self, pi, ki, ii, EH='E'):

        if EH == 'E':
            for ti, tid in enumerate(self.tx_ids[pi]):
                self.J_local[pi][:, ti, ki, ii] =\
                    np.einsum('ij,ij->i', self.GE[tid, :, :],
                              self.vecr[self.gids]) +\
                    1j * np.einsum('ij,ij->i', self.GE[tid, :, :],
                                   self.veci[self.gids])

        if EH == 'H':
            for ti, tid in enumerate(self.tx_ids[pi]):
                self.J_local[pi][:, ti, ki, ii] =\
                    np.einsum('ij,ij->i', self.GH[tid, :, :],
                              self.vecr[self.gids]) +\
                    1j * np.einsum('ij,ij->i', self.GH[tid, :, :],
                                   self.veci[self.gids])

    def prepare_jacobi_fill(self):

        self.e_ids, self.h_ids = [[], [], []], [[], [], []]
        self.AQe_real, self.AQe_imag = [], []
        self.AQh_real, self.AQh_imag = [], []
        self.fill_ids = np.arange(self.q_func.vector().local_size(),
                                  dtype=PETSc.IntType)
        dofmap = np.array(self.PP.FS.M.dofmap().dofs(),
                          dtype=PETSc.IntType)

        for pi in range(self.PP.MP.n_rx):
            e_counter, h_counter = 0, 0
            if any(val in self.components[pi] for val in
                    ['Ex', 'Ey', 'Ez']):
                e_obs = len(self.PP.MP.rx[pi]) *\
                        len(self.components[pi])
            else:
                e_obs = 0

            if any(val in self.components[pi] for val in
                    ['Hx', 'Hy', 'Hz', 'Bx', 'By', 'Bz']):
                h_obs = len(self.PP.MP.rx[pi]) *\
                        len(self.components[pi])
            else:
                h_obs = 0

            if e_obs != 0 and h_obs != 0:
                lp(self.PP.MP.logger, 50,
                   'Error! Currently, E and H fields are not allowed on the '
                   'same Rx path with *matvec* implementation. Aborting  ...',
                   pre_dash=False)
                raise SystemExit

            if df.MPI.rank(df.MPI.comm_world) == 0:

                self.J.append(np.zeros((len(self.inv_ids), e_obs +
                                        h_obs), dtype=complex))
            else:
                pass
            df.MPI.barrier(df.MPI.comm_world)

            if 'Ex' in self.components[pi]:
                self.e_ids[0].append(np.arange(e_counter, e_counter +
                                               len(self.PP.MP.rx[pi])))
                e_counter = self.e_ids[0][-1][-1] + 1
            else:
                self.e_ids[0].append([])
            if 'Ey' in self.components[pi]:
                self.e_ids[1].append(np.arange(e_counter, e_counter +
                                               len(self.PP.MP.rx[pi])))
                e_counter = self.e_ids[1][-1][-1] + 1
            else:
                self.e_ids[1].append([])
            if 'Ez' in self.components[pi]:
                self.e_ids[2].append(np.arange(e_counter, e_counter +
                                               len(self.PP.MP.rx[pi])))
                e_counter = self.e_ids[2][-1][-1] + 1
            else:
                self.e_ids[2].append([])
            if 'Hx' in self.components[pi] or 'Bx' in self.components[pi]:
                self.h_ids[0].append(np.arange(h_counter, h_counter +
                                               len(self.PP.MP.rx[pi])))
                h_counter = self.h_ids[0][-1][-1] + 1
            else:
                self.h_ids[0].append([])
            if 'Hy' in self.components[pi] or 'By' in self.components[pi]:
                self.h_ids[1].append(np.arange(h_counter, h_counter +
                                               len(self.PP.MP.rx[pi])))
                h_counter = self.h_ids[1][-1][-1] + 1
            else:
                self.h_ids[1].append([])
            if 'Hz' in self.components[pi] or 'Bz' in self.components[pi]:
                self.h_ids[2].append(np.arange(h_counter, h_counter +
                                               len(self.PP.MP.rx[pi])))
                h_counter = self.h_ids[2][-1][-1] + 1
            else:
                self.h_ids[2].append([])

            # set up matrices
            rgmap = PETSc.LGMap().create(dofmap, comm=PETSc.COMM_WORLD)
            e_cgmap = PETSc.LGMap().create(range(e_obs),
                                           comm=PETSc.COMM_WORLD)
            h_cgmap = PETSc.LGMap().create(range(h_obs),
                                           comm=PETSc.COMM_WORLD)

            if e_obs > 0:
                self.AQe_real.append(PETSc.Mat())
                self.AQe_real[-1].create(PETSc.COMM_WORLD)
                self.AQe_real[-1].setSizes([[len(self.PP.FS.M.dofmap().dofs()),
                                           self.PP.FS.M.dim()],
                                           [e_obs, e_obs]])
                self.AQe_real[-1].setType('dense')
                self.AQe_real[-1].setPreallocationDense(None)
                self.AQe_real[-1].setLGMap(rgmap, e_cgmap)

                self.AQe_imag.append(PETSc.Mat())
                self.AQe_imag[-1].create(PETSc.COMM_WORLD)
                self.AQe_imag[-1].setSizes([[len(self.PP.FS.M.dofmap().dofs()),
                                           self.PP.FS.M.dim()],
                                           [e_obs, e_obs]])
                self.AQe_imag[-1].setType('dense')
                self.AQe_imag[-1].setPreallocationDense(None)
                self.AQe_imag[-1].setLGMap(rgmap, e_cgmap)
            else:
                self.AQe_real.append(None)
                self.AQe_imag.append(None)
            if h_obs > 0:
                self.AQh_real.append(PETSc.Mat())
                self.AQh_real[-1].create(PETSc.COMM_WORLD)
                self.AQh_real[-1].setSizes([[len(self.PP.FS.M.dofmap().dofs()),
                                           self.PP.FS.M.dim()],
                                           [h_obs, h_obs]])
                self.AQh_real[-1].setType('dense')
                self.AQh_real[-1].setPreallocationDense(None)
                self.AQh_real[-1].setLGMap(rgmap, h_cgmap)

                self.AQh_imag.append(PETSc.Mat())
                self.AQh_imag[-1].create(PETSc.COMM_WORLD)
                self.AQh_imag[-1].setSizes([[len(self.PP.FS.M.dofmap().dofs()),
                                           self.PP.FS.M.dim()],
                                           [h_obs, h_obs]])
                self.AQh_imag[-1].setType('dense')
                self.AQh_imag[-1].setPreallocationDense(None)
                self.AQh_imag[-1].setLGMap(rgmap, h_cgmap)
            else:
                self.AQh_real.append(None)
                self.AQh_imag.append(None)

    def magnetic_factorization(self, fi, solvingapproach='direct'):

        lp(self.PP.MP.logger, 20,
            '...  factorizing additional system for H-field Jacobian '
            'entries  ...', pre_dash=False)

        # reduce log level to avoid unnecessary prints
        ll = int(self.PP.MP.logger.handlers[0].level)
        if ll < 30:
            self.PP.MP.logger.handlers[0].setLevel(30)

        if not hasattr(self, 'HFE'):
            self.create_magnetic_form()

        # create system for dummy solve with arbitrary rhs
        for ti in range(self.HFE.n_tx):
            if ti == 0:
              if self.HFE.MP.system_it==False:
                  self.A = df.PETScMatrix()
                  df.assemble_system(self.HFE.L[fi], self.HFE.R[ti],
                                     A_tensor=self.A, b_tensor=self.b[ti])
                
              if self.HFE.MP.system_it==True: 
                  self.Asys = df.PETScMatrix()
                  self.Hh= df.PETScMatrix()
                  self.Adiag= df.PETScMatrix()
                  self.Aoffdiag= df.PETScMatrix()
                  df.assemble_system(self.HFE.L[fi], self.HFE.R[ti],
                                     A_tensor=self.Asys, b_tensor=self.b[ti])
                  df.assemble(self.HFE.H[fi], tensor=self.Hh)
                  df.assemble(self.HFE.A[fi], tensor=self.Adiag)
                  df.assemble(self.HFE.B[fi], tensor=self.Aoffdiag)                   
            else:
              df.assemble(self.HFE.R[ti], tensor=self.b[ti])   
                 

        H_solver = RawSolver(self.PP.FS, self.HFE,
                             mumps_debug=self.PP.MP.mumps_debug,
                             serial_ordering=self.PP.MP.serial_ordering)
        
                         
        if solvingapproach == 'direct':                     
          H_solver.solve_system_mumps(
                    self.A, self.b, self.HFE.FS.M, sym=True)
          self.PP.MP.logger.handlers[0].setLevel(ll)
          return H_solver          
        if solvingapproach == 'iterative':  
          # Replace by PRESB        
          H_solver.solve_system_iter_PRESB(
                    self.Asys, self.Hh, self.b, self.Aoffdiag,
                    self.Adiag, inner='iterative',
                    method='fgmres', flag_mt=False,
                    fs=self.HFE.FS.M)          
          # switch back to specified debug level of main model
          self.PP.MP.logger.handlers[0].setLevel(ll)
          return H_solver,self.Asys,self.Hh,self.Aoffdiag,self.Adiag
        


# # # # # # # # # # # # # # # # # old stuff # # # # # # # # # # # # # # # # # #

    # def solve_untransposed_magnetic(self, Solver, fi):

    #     # calc J as Q * A^-1 * G
    #     JQ = df.Function(self.PP.FS.M)

    #     # for pi in range(self.PP.MP.n_rx):
    #     #     for ti in self.tx_ids[pi]:
    #     #         for j in range(len(self.dA)):
    #     #             if pi == 0:
    #     #                 t000 = time.time()
    #     #                 sinv2 = -1. / self.PP.MP.sigma[self.inv_ids[j]][0]**2
    #     #                 # multiply with sigma if "log_scaled"
    #     #                 sig0 = self.PP.MP.sigma_0[self.inv_ids[j]-1][0]
    #     #                 if df.MPI.rank(df.MPI.comm_world) == 0:
    #     #                     print(self.PP.MP.sigma[self.inv_ids[j]][0],
    #     #                           self.PP.MP.sigma_0[self.inv_ids[j]-1][0])
    #     #                 G.vector().set_local(self.dA[j] *
    #     #                                      self.PP.FS.U[ti].vector())
    #     #                 G.vector()[:] = self.db[fi][j][ti][:] * sinv2 * sig0 -\
    #     #                                 G.vector()[:] * sinv2
    #     #                 if df.MPI.rank(df.MPI.comm_world) == 0:
    #     #                     print(time.time() - t000)

    #     #                 Solver.solver.solve(JQ.vector(), G.vector())
    #     #                 Jhr, Jhi = JQ.split(True)

    #     #             for k in range(len(self.PP.MP.rx[pi])):
    #     #                 i = 0
    #     #                 if 'H_x' in self.components[pi]:
    #     #                     self.J[pi][j, ti, k, i] = \
    #     #                         self.IB.Q[pi][k][0].vector().inner(
    #     #                             Jhr.vector()) + \
    #     #                         1j * self.IB.Q[pi][k][0].vector().inner(
    #     #                             Jhi.vector())
    #     #                     i += 1
    #     #                 if 'H_y' in self.components[pi]:
    #     #                     self.J[pi][j, ti, k, i] = \
    #     #                         self.IB.Q[pi][k][1].vector().inner(
    #     #                             Jhr.vector()) + \
    #     #                         1j * self.IB.Q[pi][k][1].vector().inner(
    #     #                             Jhi.vector())
    #     #                     i += 1
    #     #                 if 'H_z' in self.components[pi]:
    #     #                     self.J[pi][j, ti, k, i] = \
    #     #                         self.IB.Q[pi][k][2].vector().inner(
    #     #                             Jhr.vector()) + \
    #     #                         1j * self.IB.Q[pi][k][2].vector().inner(
    #     #                             Jhi.vector())

    #     for ai in range(len(self.inv_ids)):
    #         for ti in range(self.PP.MP.n_tx):
    #             G = self.create_Gh_from_db(ai)
    #             Solver.solver.solve(JQ.vector(), G[ti].vector())
    #             Jhr, Jhi = JQ.split(True)

    #             for pi in range(self.PP.MP.n_rx):
    #                 tx_counter = 0
    #                 if ti in self.tx_ids[pi]:
    #                     for k in range(len(self.PP.MP.rx[pi])):
    #                         i = 0
    #                         if 'H_x' in self.components[pi]:
    #                             self.J[pi][ai, tx_counter, k, i] = \
    #                                 self.IB.Q[pi][k][0].vector().inner(
    #                                     Jhr.vector()) + \
    #                                 1j * self.IB.Q[pi][k][0].vector().inner(
    #                                     Jhi.vector())
    #                             i += 1
    #                         if 'H_y' in self.components[pi]:
    #                             self.J[pi][ai, tx_counter, k, i] = \
    #                                 self.IB.Q[pi][k][1].vector().inner(
    #                                     Jhr.vector()) + \
    #                                 1j * self.IB.Q[pi][k][1].vector().inner(
    #                                     Jhi.vector())
    #                             i += 1
    #                         if 'H_z' in self.components[pi]:
    #                             self.J[pi][ai, tx_counter, k, i] = \
    #                                 self.IB.Q[pi][k][2].vector().inner(
    #                                     Jhr.vector()) + \
    #                                 1j * self.IB.Q[pi][k][2].vector().inner(
    #                                     Jhi.vector())
    #                     tx_counter += 1

    # def solve_transposed_magnetic(self, Solver, fi):

    #     # calc J as G^T * A^-1 * Q^T
    #     JGr, JGi = df.Function(self.PP.FS.M), df.Function(self.PP.FS.M)
    #     G_vectors = []

    #     for ai in range(len(self.dA)):
    #         G_vectors.append(self.create_Gh_from_db(ai))

    #     for pi in range(self.PP.MP.n_rx):
    #         for k in range(len(self.PP.MP.rx[pi])):
    #             if df.MPI.rank(df.MPI.comm_world) == 0:
    #                 print(k)
    #             i = 0
    #             if 'H_x' in self.components[pi]:
    #                 Solver.solver.solve(JGr.vector(),
    #                                     self.Qr[pi][k][0].vector())
    #                 Solver.solver.solve(JGi.vector(),
    #                                     self.Qi[pi][k][0].vector())

    #                 for ti in self.tx_ids[pi]:
    #                     for j in range(len(self.dA)):
    #                         self.J[pi][j, ti, k, i] = \
    #                                     G_vectors[j][ti].vector().inner(
    #                                         JGr.vector()) + \
    #                                     1j * G_vectors[j][ti].vector().inner(
    #                                         JGi.vector())
    #                 i += 1

    #             if 'H_y' in self.components[pi]:
    #                 Solver.solver.solve(JGr.vector(),
    #                                     self.Qr[pi][k][1].vector())
    #                 Solver.solver.solve(JGi.vector(),
    #                                     self.Qi[pi][k][1].vector())

    #                 for ti in self.tx_ids[pi]:
    #                     for j in range(len(self.dA)):
    #                         self.J[pi][j, ti, k, i] = \
    #                                     G_vectors[j][ti].vector().inner(
    #                                         JGr.vector()) + \
    #                                     1j * G_vectors[j][ti].vector().inner(
    #                                         JGi.vector())
    #                 i += 1

    #             if 'H_z' in self.components[pi]:
    #                 Solver.solver.solve(JGr.vector(),
    #                                     self.Qr[pi][k][2].vector())
    #                 Solver.solver.solve(JGi.vector(),
    #                                     self.Qi[pi][k][2].vector())

    #                 for ti in self.tx_ids[pi]:
    #                     for j in range(len(self.dA)):
    #                         self.J[pi][j, ti, k, i] = \
    #                                     G_vectors[j][ti].vector().inner(
    #                                         JGr.vector()) + \
    #                                     1j * G_vectors[j][ti].vector().inner(
    #                                         JGi.vector())

    def eval_inv_conditions(self):

        # eval which method (transposed or not) is faster and specify it
        n_rx = 0
        for ci, coords in enumerate(self.PP.MP.rx):
            n_rx += len(coords) * len(self.components[ci])

        if n_rx < self.PP.FS.DOM.n_domains - len(self.skip_domains):
            self.solve_transpose = True
        else:
            self.solve_transpose = False

        # eval which domains to take care of
        if len(self.inv_ids) < 1:
            for di in range(self.PP.FS.DOM.n_domains):
                if di in self.skip_domains:
                    # skip airspace and others if specifed
                    continue
                else:
                    self.inv_ids.append(di)

    def get_mask(self, inv_dir, fi):

        if os.path.isfile(inv_dir + 'masks/jacobi_mask_f' + str(fi) + '.npz'):
            container = np.load(inv_dir + 'masks/jacobi_mask_f' +
                                str(fi) + '.npz', allow_pickle=True)
            self.mask = [container[key] for key in container]
        else:
            self.mask = []
            for pi in range(self.PP.MP.n_rx):
                l_cmp = self.get_cmp_size(pi)
                self.mask.append(np.ones((len(self.tx_ids[pi]),
                                          len(self.PP.MP.rx[pi]),
                                          l_cmp), dtype=bool))

    def get_cmp_size(self, pi):

        if self.PP.MP.approach == 'MT':
            if self.e_fields and not self.tipper:  # Z only
                return(4)
            elif self.e_fields and self.h_fields:  # Z and Tipper
                return(5)
            else:  # Tipper only
                return(3)
        else:
            return(len(self.components[pi]))

    def eval_mt_components(self):

        self.mt_cmp = []
        for pi in range(self.PP.MP.n_rx):
            self.mt_cmp.append([])
            if any(val in self.components[pi] for val in
               ['Zxx', 'Zxy', 'Zyx', 'Zyy']):
                self.mt_cmp[pi].append('Z')
            if 'Tx' in self.components[pi] or 'Ty' in self.components[pi]:
                self.mt_cmp[pi].append('T')
            if len(self.mt_cmp[pi]) == 0:
                self.mt_cmp[pi].append(None)

    def create_magnetic_form(self):

        HMP = ModelParameters(self.PP.MP.logger,
                              [self.PP.MP.mod_name,
                               self.PP.MP.mesh_name,
                               'H_s', self.PP.MP.file_format,
                               self.PP.MP.m_dir, self.PP.MP.r_dir,
                               self.PP.MP.para_dir, self.PP.MP.out_dir,
                               self.PP.MP.mute, self.PP.MP.mpi_cw,
                               self.PP.MP.mpi_cs, self.PP.MP.mpi_rank,
                               False, False, False, True,
                               self.PP.MP.mumps_debug,
                               self.PP.MP.serial_ordering,
                               self.PP.MP.self_mode],
                              self.PP.MP.test_mode,
                              self.PP.MP.solvingapproach)

        HMP.n_domains = self.PP.FS.DOM.n_domains
        HMP.update_model_parameters(
            frequencies=self.PP.MP.frequencies,
            sigma_ground=self.PP.MP.sigma_ground,
            sigma_0=self.PP.MP.sigma_ground)

        self.HFE = H_vector(self.PP.FS, HMP)
        self.HFE.build_var_form(add_primary=False)
        self.HFE.R = []
        for ti in range(self.PP.MP.n_tx):
            # use dummy source
            self.HFE.R.append(
                df.inner(df.Constant(("1.0", "0.0", "0.0")),
                         self.HFE.v_r) * self.HFE.FS.DOM.dx_0 +
                df.inner(df.Constant(("0.0", "0.0", "0.0")),
                         self.HFE.v_i) * self.HFE.FS.DOM.dx_0)

        self.b = [df.PETScVector() for n in range(self.HFE.n_tx)]
