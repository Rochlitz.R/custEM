# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import numpy as np
import pygimli as pg
import os
import sys
import json
import time
import custEM as cu
from mpi4py import MPI
from custEM.core import MOD
from subprocess import Popen
import matplotlib.pyplot as plt
from matplotlib.colors import SymLogNorm
from matplotlib.colors import LogNorm
from matplotlib.backends.backend_pdf import PdfPages
#from saem import CSEMData


class MultiFWD(pg.Modelling):

    """
    Modified multi-forward-modeling class on basis of *pg.Modelling* to
    integrate custEM into pyGIMLi. Class is designed to run multiple
    MPI/OpenMP-parallelized threads for a specific amount of frequencies in
    frequency-domain EM simulations at the same time.

    Methods
    -------

    - response()
        overwrite build-in *response* method of *pg.Modelling* class to
        provide EM fields or MT data via custEM simulations

    - createJacobian()
        overwrite build-in *createJacobian* method of *pg.Modelling* class to
        provide Jacobian via custEM simulations

    - eval_parallel_layout()
        evaluate parallel layout based on keyword arguments to optimize
        performance on individual computer architectures

    - init_mesh()
        import forward modeling mesh and cut the inner part as inversion mesh

    - init_inv_progress()
        initialize progress of previous iterations if existing

    - run_multi_fwds()
        run multiple simulations and Jacobian calculations simultaneously

    - import_fields()
        import fields after forward simulation

    - import_mt()
        import MT data after forward simulation

    - import_jacobian()
        import Jacobian after forward/Jacobian calculation

    - import_sub_jacobianan()
        merge sub-jacobians based on MPI parallelization

    - stack_jacobianan()
        succesively build jacobian column vector

    - analyze()
        save chi^2, response and resistivity model after each iteration

    - convert_response_to_npz()
        save computed synthetic data with **MultiFWD** class in column format
        to an *.npz* file which to enable importing data in an inversion
        or plotting script with the *saemdata* format
    """

    def __init__(self, mod, mesh,
	               freqs=None, cmps=None, tx_ids=None,
                 saem_data=None, sig_bg=0.001,  sig_0=None, skip_domains=[0, 1],
                 p_fwd=1, approach='E_t', sig_fix=None,
                 max_procs=None, min_freqs=None, n_cores=144,
                 start_iter=0, inv_type='cells', print_interval=200,
                 bind_procs=False, n_layers=None, save_pvd=False,
                 set_zero_markers=True, converted_mod=None,
                 magarrow=False, remote_station=None,
                 solvingapproach=['direct','MUMPS','iterative','H_direct']):

        """
        Initialize the forward modeling instance. There are two options to
        provide the following information: Specify either *freqs*,
        *cmps* and *tx_ids* or *saem_data* (automated detection).

        Required arguments
        ------------------

        - mod, type str
            inversion model name

        - mesh, type str
            inversion mesh name

        Keyword arguments
        -----------------

        - freqs = None, type list of float
            frequencies to calculate for all patches

        - cmps = None, type list of list of str
            components to calculate for each patch

        - tx_ids = None, type list of list of int
            transmitter ids to use to calculate sensitivities for each patch

        - saem_data = None, type numpy object
            import numpy data container for automated detection which was
            created with the SAEM class

        - sig_b = 0.001, type float
            homogeneous starting model as conductivity

        - skip_domains = [1, 0], list of int
            domain markers for domains outside of the inversion domain which
            should not be overwritten during the TetGen mesh import

        - p_fwd = 1, type int
            polynomial order to solve forward problem (either *1* or *2*)

        - approach = 'E_t', type str
            do not change for now (we considering also E_s to work, but not
                                   implemented yet)

        - max_procs = None, type int
            maximum number of MPI-processes to use for each forward modeling
            thread, this keywork argument has higher priority than *n_cores*

        - min_freqs = None, type int
            specify, if forward modeling threads should calculate multiple
            frequencies, default is 1 frequency per thread, but could be, e.g.,
            2 frequencies per thread to double computations times with half
            of the memory requirements

        - n_cores = 72, type int
            if *max_procs* is *None*, automatically choose suited number of
            MPI processes for each forward modeling thread based on the
            available (specified) total number of cores, e.g., 12 frequencies
            with 72 cores and *min_freqs* = None will use 6 cores per thread

        - start_iter = 0, type int
            if there are succefull previous inversion iterations,
            start at the provided iteration number and not from the beginning

        - inv_type = 'cells', type str
            choose either *cells* for 3D or *domains* for 2.5D inversion

        - print_interval = 10,
            print forward modeling status every *print_interval* seconds

        - bind_procs = False,
            for development and debugging purposes only

        - n_layers = None, type int
            for development and debugging purposes only

        - save_pvd = False, type bool
            for development and debugging purposes only

        - set_zero_markers = True, type bool
            for development and debugging purposes only
        """

        super().__init__()
        print('  -  Starting inversion of model "' + mod + '" on mesh "' +
              mesh + '"  -  ')
        self.export_dir = 'results/' + approach + '/' + mesh + '/' + mod
        self.inv_dir = 'inv_results/' + mod + '_' + mesh + '/'
        if not os.path.isdir(self.inv_dir):
            os.makedirs(self.inv_dir)
        if not os.path.isdir(self.inv_dir + 'masks/'):
            os.makedirs(self.inv_dir + 'masks/')

        if saem_data is not None:
            if freqs is not None or cmps is not None or \
                tx_ids is not None:
                print('Error! Specify either *saem_data* or '
                      '*cmps*, *freqs*, and *tx_ids*. Aborting  ...')
                raise SystemExit

            freqs = [freq for freq in saem_data["freqs"]]
            cmps = [data["cmp"] for data in saem_data["DATA"]]
            tx_ids = [data["tx_ids"] for data in saem_data["DATA"]]
            data_r, data_i = [], []
            error_r, error_i = [], []
            self.jacobi_mask = [[] for fi in range(len(freqs))]

            for i, data in enumerate(saem_data["DATA"]):
                data_r = np.concatenate([data_r, data["dataR"].ravel()])
                data_i = np.concatenate([data_i, data["dataI"].ravel()])
                error_r = np.concatenate([error_r, data["errorR"].ravel()])
                error_i = np.concatenate([error_i, data["errorI"].ravel()])
                self.generate_jacobi_mask(freqs, cmps, data, approach)

            for fi in range(len(freqs)):
                np.savez(self.inv_dir + 'masks/jacobi_mask_f' +
                         str(fi) + '.npz', *self.jacobi_mask[fi])
            del self.jacobi_mask

            self.data_mask = np.isfinite(data_r + data_i + error_r + error_i)
            self.measured = np.hstack((data_r[self.data_mask],
                                       data_i[self.data_mask]))

            self.errors = np.abs(np.hstack((error_r[self.data_mask],
                                            error_i[self.data_mask])) /
                                 self.measured)

            alldata = np.hstack((data_r, data_i))
            np.save(self.inv_dir + 'datavec.npy', alldata)
            np.save(self.inv_dir + 'errorvec.npy',
                    np.abs(np.hstack((error_r, error_i)) / (alldata + 1e-12)))
            np.save(self.inv_dir + 'mask.npy', self.data_mask)
        else:
            self.data_mask = None

        for cmp in cmps:
            if any('B' in elem for elem in cmp):
                self.b_fields = True
            else:
                self.b_fields = False

        self.mu_nt = np.pi * 400.
        self.inv_type = inv_type
        self.freqs = freqs
        self.n_freqs = len(freqs)
        self.cmps = cmps
        self.tx_ids = tx_ids
        self.sig_bg = sig_bg
        if sig_fix is None:
            self.sig_fix = [sig_bg] * (len(skip_domains) - 1)
        else:
            self.sig_fix = sig_fix
        self.n_cores = n_cores
        self.n_layers = n_layers
        self.iteration = start_iter
        self.print_interval = print_interval
        self.magarrow = magarrow

        self.config = dict()
        self.config['mod'] = mod
        self.config['mesh'] = mesh
        self.config['freqs'] = freqs
        self.config['components'] = cmps
        self.config['tx_ids'] = tx_ids
        self.config['p_fwd'] = p_fwd
        self.config['skip_domains'] = skip_domains
        self.config['save_pvd'] = save_pvd
        self.config['approach'] = approach
        self.config['solvingapproach0'] = solvingapproach[0]
        self.config['solvingapproach1'] = solvingapproach[1]
        self.config['solvingapproach2'] = solvingapproach[2]
        self.config['solvingapproach3'] = solvingapproach[3]
        self.config['remote_station'] = remote_station
        self.bind_procs = bind_procs
        

        self.init_mesh(mod, mesh, approach, set_zero_markers, converted_mod)
        self.init_inv_progress()
        self.eval_parallel_layout(max_procs, min_freqs)

        if start_iter > 0:
            self.sig_0 = np.load(self.inv_dir + 'sig_iter_' +
                                  str(self.iteration) + '.npy')
            print('  -  starting at iteration ' + str(start_iter) + '  - ')
        else:
            if sig_0 is None:
                self.sig_0 = sig_bg
            else:
                self.sig_0 = sig_0
        if hasattr(self, 'measured'):
            self._jac = pg.Matrix(len(self.measured),
                                  self.n_domains - len(skip_domains))
        else:
            self._jac = pg.Matrix()
        self.setJacobian(self._jac)


    def response(self, sig):

        """
        Call methods to calculate and import the response
        as return argument for the pygimli inversion.
        """
        
        self.run_multi_fwds(sig, None)
        if self.config['approach'] == 'E_t':
            self.import_fields()
        elif self.config['approach'] == 'MT':
            self.import_mt()

        return(self.last_response)

    def createJacobian(self, sig):

        """
        Call methods to calculate and import the jacobian
        as return argument for the pygimli inversion.
        """

        self.run_multi_fwds(sig, self.inv_type)
        self.iteration += 1
        if self.config['approach'] == 'E_t':
            self.import_fields()
        elif self.config['approach'] == 'MT':
            self.import_mt()

        self.import_jacobian()
        self._jac.resize(*self.J.shape)
        for i, row in enumerate(self.J):
            self._jac.setRow(i, row)

    def eval_parallel_layout(self, max_procs, min_freqs):

        """
        Set up suited parallel layout depending on the workstation
        architecture. The argument *max_procs* has always priority. If not
        specified, the total available number of cores *n_cores* will be
        split into the maximum available number of cores for each job.
        If *min_freqs* is not adjusted, the problem will be split into
        *n_freqs* jobs.
        """

        if self.n_cores < self.n_freqs:
            print('Error! Currently, inversion requires at least 1 core per '
                  'frequency. Aborting  ...')
            raise SystemExit

        if min_freqs is None:
            self.n_threads = self.n_freqs
            self.config['calc_freqs'] = [[fi] for fi in range(self.n_freqs)]
        else:
            if min_freqs == 2 and min_freqs < self.n_freqs:
                self.n_threads = int(self.n_freqs / 2) 
            elif min_freqs == 3 and min_freqs < self.n_freqs:
                self.n_threads = int(self.n_freqs / 3) 
            elif min_freqs == 4 and min_freqs < self.n_freqs:
                self.n_threads = int(self.n_freqs / 4) 
            elif min_freqs == self.n_freqs:
                self.n_threads = 1
            else:
                print('Error! Currently, *min_freqs* must be smaller than 4 '
                      'or equal to *n_freqs*. Aborting  ...')
                raise SystemExit
            counter = 0
            self.config['calc_freqs'] = []
            while counter < self.n_freqs:
                self.config['calc_freqs'].append(
                    [fi for fi in range(counter, counter + (min_freqs))])
                counter += (min_freqs)
        
        if max_procs is None:
            self.n_procs = int(self.n_cores / self.n_threads)
        else:
            self.n_procs = max_procs

        # old hardcoded lines for testing, do not use without modification
        if self.bind_procs:
            self.proc_ids = ['0-15,128-143',
                              '16-31,144-159',
                              '32-47,160-175',
                              '48-63,176-191',
                              '64-79,192-207',
                              '80-95,208-223',
                              '96-111,224-239',
                              '112-127,240-255']

    def init_mesh(self, mod, mesh, approach, set_zero_markers,
                  converted_mod):

        """
        Import and convert TetGen mesh in serial. Overwrite specific
        markers specifying the inversion domain depending on 2.5D or 3D
        inversion and export the inversion mesh.
        """

        if converted_mod is None:
            # convert mesh and create export directories from root
            M = MOD(mod, mesh, approach, overwrite_mesh=True, mesh_only=True,
                    overwrite_results=True, export_domains=False)
        else:
            M = converted_mod

        if self.inv_type == 'cells':
            h5mesh = pg.meshtools.readHDF5Mesh(
                'meshes/_h5/' + mesh + '.h5', group='domains',
                indices='cell_indices', pos='coordinates', cells='topology',
                marker='values', marker_default=0, dimension=3,
                verbose=True, useFenicsIndices=False)
            pgmesh = pg.Mesh(3)
            pgmesh.createMeshByMarker(h5mesh, len(self.config['skip_domains']))
            if set_zero_markers:
                pgmesh.setCellMarkers(pg.Vector(pgmesh.cellCount()))
        elif self.inv_type == 'domains':
            pgmesh = pg.load(M.MP.m_dir + '/mesh_create/' + mesh + '.bms')
        else:
            pgmesh = None

        if self.inv_type == 'layers':
            self.n_domains = self.n_layers
            self.n_res = self.n_layers
            self.mesh1d = pg.meshtools.createMesh1D(self.n_res)
            self.setMesh(self.mesh1d)
        if self.inv_type == 'domains':
            self.n_domains = M.FS.DOM.n_domains
            self.n_res = self.n_domains - len(self.config['skip_domains'])
            self.setMesh(pg.load(M.MP.m_dir + '/mesh_create/' + mesh + '.bms'))
        if self.inv_type == 'cells':
            self.n_domains = M.FS.DOM.n_domains
            self.n_res = self.n_domains - len(self.config['skip_domains'])
            self.setMesh(pgmesh)

        self.mesh().exportVTK(self.inv_dir + 'invmesh.vtk')
        self.rxs = M.MP.rx
        self.txs = M.MP.tx
        self.n_rx = M.MP.n_rx
        self.n_tx = M.MP.n_tx

    def init_inv_progress(self):

        self.iters, self.chi2 = [], []
        if self.iteration > 0:
            tmp = np.atleast_2d(
                np.loadtxt(self.inv_dir + 'chi2.dat', dtype=str))
            for j in range(self.iteration):
                self.iters.append(tmp[j, 0] + ' ' + tmp[j, 1] + ' ' +
                                  tmp[j, 2])
                self.chi2.append(float(tmp[j, 3]))

    def run_multi_fwds(self, sig, jacobian):

        """
        Forward response.
        """

        self.config['jacobian'] = jacobian
        self.config['iteration'] = self.iteration
        self.config['inv_dir'] = self.inv_dir
        if type(sig) is list:
            self.sig = np.array(sig)
        elif type(sig) is np.ndarray:
            self.sig = sig
        else:
            self.sig = sig.array()

        for i, idx in enumerate(self.config['skip_domains'][1:]):
            self.sig = np.insert(self.sig, idx-1, self.sig_fix[i])

        self.config['sig'] = self.sig.tolist()

        with open(self.export_dir + '_inv_config_' +
                  str(self.iteration) + '.json', "w") as outfile:
            json.dump(self.config, outfile, indent=0)

        custem_path = os.path.dirname(cu.__file__) + '/inv/'
        if self.bind_procs:
            to_call = ['bash', custem_path + 'run_fwd_bind.sh']
        else:
            to_call = ['bash', custem_path + 'run_fwd.sh']

        to_call.append(custem_path + 'run_fwd_from_config.py')
        to_call.append(self.export_dir + '_inv_config_' +
                       str(self.iteration) + '.json')
        to_call.append(str(self.n_procs))
        to_call.append(str(0))

        if self.bind_procs:
            start_proc = 0
            to_call.append(self.proc_ids[0])
        
        jobs = []
        t00 = time.time()
        for thread_id in range(self.n_threads):
            if thread_id != 0:
                if self.bind_procs:
                    to_call[-2] = str(thread_id)
                    start_proc += self.n_procs
                    to_call[-1] = self.proc_ids[thread_id]
                else:
                    to_call[-1] = str(thread_id)
          
            jobs.append(Popen(to_call, stdout=sys.stdout, stderr=sys.stdout))
            # wait a bit to avoid delays from simultaneous module imports
            time.sleep(1.)

        running = [jobs[job].poll() for job in range(self.n_threads)]
        while None in running:
            time.sleep(1.)
            running = [jobs[job].poll() for job in range(self.n_threads)]
            if (int(time.time() - t00) % self.print_interval) == 0:
                print('...  working  -->  time elapsed (s): ',
                      '{:.2f}'.format(time.time() - t00),
                      ' ...\n  - ', running, ' -  ')
            if 1 in running:
                thread_id = running.index(1)
                print('RESTARTING', running.index(1))
                print('Increasing n_procs by 2')
                to_call[-1] = str(thread_id)
                # raise n_procs for restart to speed up a bit
                to_call[-2] = str(self.n_procs + 2)
                jobs[thread_id] = Popen(to_call,
                                        stdout=sys.stdout, stderr=sys.stdout)

    def import_fields(self):

        fields = np.array([], dtype=complex)
        for pi in range(self.n_rx):
            E = np.zeros((len(self.tx_ids[pi]), self.n_freqs,
                         len(self.rxs[pi]), 3), dtype=complex)
            H = np.zeros((len(self.tx_ids[pi]), self.n_freqs,
                         len(self.rxs[pi]), 3), dtype=complex)

            #x=[]
            
            #for xs in self.config['calc_freqs']:
              #for xss in xs:
                #x.append(xss)
            
            for ti in range(len(self.tx_ids[pi])):
                thread_id = 0
                for fi in range(self.n_freqs):
                #for fi in x:
                    try:
                        E[:, fi, :, :] = np.load(
                            self.export_dir + '_thread_' + str(thread_id) +
                            '_interpolated/f_' + str(fi) +
                            '_E_t_all_tx_rx_path_' +
                            str(pi) + '.npy')[self.tx_ids[pi][ti], :, :]
                        H[:, fi, :, :] = np.load(
                            self.export_dir + '_thread_' + str(thread_id) +
                            '_interpolated/f_' + str(fi) +
                            '_H_t_all_tx_rx_path_' +
                            str(pi) + '.npy')[self.tx_ids[pi][ti], :, :]
                    except FileNotFoundError:
                        thread_id += 1
                        E[:, fi, :, :] = np.load(
                            self.export_dir + '_thread_' + str(thread_id) +
                            '_interpolated/f_' + str(fi) +
                            '_E_t_all_tx_rx_path_' +
                            str(pi) + '.npy')[self.tx_ids[pi][ti], :, :]
                        H[:, fi, :, :] = np.load(
                            self.export_dir + '_thread_' + str(thread_id) +
                            '_interpolated/f_' + str(fi) +
                            '_H_t_all_tx_rx_path_' +
                            str(pi) + '.npy')[self.tx_ids[pi][ti], :, :]

                if self.b_fields:
                    H *= self.mu_nt

                if not self.magarrow:
                    if 'Ex' in self.cmps[pi]:
                        fields = np.append(fields, E[ti, :, :, 0].ravel())
                    if 'Ey' in self.cmps[pi]:
                        fields = np.append(fields, E[ti, :, :, 1].ravel())
                    if 'Ez' in self.cmps[pi]:
                        fields = np.append(fields, E[ti, :, :, 2].ravel())

                    # if not self.magarrow:
                    if 'Hx' in self.cmps[pi] or 'Bx' in self.cmps[pi]:
                        fields = np.append(fields, H[ti, :, :, 0].ravel())
                    if 'Hy' in self.cmps[pi] or 'By' in self.cmps[pi]:
                        fields = np.append(fields, H[ti, :, :, 1].ravel())
                    if 'Hz' in self.cmps[pi] or 'Bz' in self.cmps[pi]:
                        fields = np.append(fields, H[ti, :, :, 2].ravel())

                else:
                    scale = np.linalg.norm([self.east, self.north, self.up])
                    NEDdata = np.zeros((3, len(H[ti, :, :, 0].ravel())),
                                        dtype=complex)
                    NEDdata[0] = H[ti, :, :, 0].ravel()
                    NEDdata[1] = H[ti, :, :, 1].ravel()
                    NEDdata[2] = H[ti, :, :, 2].ravel()
                    tmp_real = np.dot(NEDdata.real.T,
                                      [self.east, self.north, self.up]) / scale
                    tmp_imag = np.dot(NEDdata.imag.T,
                                      [self.east, self.north, self.up]) / scale
                    fields = np.append(fields, tmp_real + 1j * tmp_imag)

        # np.save(self.inv_dir + 'Bz.npy', fields)

        if self.iteration == 0:
            np.save(self.inv_dir + 'response_iter_0.npy',
                    np.hstack((fields.real, fields.imag)))

        if self.data_mask is not None:
            fields =  fields[self.data_mask]
        self.last_response = np.hstack((fields.real, fields.imag))

    def import_mt(self):

        fields = np.array([], dtype=complex)
        for pi in range(self.n_rx):
            Z = np.zeros((self.n_freqs,
                         len(self.rxs[pi]), 2, 2), dtype=complex)
            T = np.zeros((self.n_freqs,
                         len(self.rxs[pi]), 2), dtype=complex)

            thread_id = 0
            for fi in range(self.n_freqs):
                try:
                    Z[fi, :, :] = np.load(
                        self.export_dir + '_thread_' + str(thread_id) +
                        '_interpolated/f_' + str(fi) + '_Z_rx_path_' +
                        str(pi) + '.npy')
                    T[fi, :, :] = np.load(
                        self.export_dir + '_thread_' + str(thread_id) +
                        '_interpolated/f_' + str(fi) + '_T_rx_path_' +
                        str(pi) + '.npy')
                except FileNotFoundError:
                    thread_id += 1
                    Z[fi, :, :] = np.load(
                        self.export_dir + '_thread_' + str(thread_id) +
                        '_interpolated/f_' + str(fi) + '_Z_rx_path_' +
                        str(pi) + '.npy')
                    T[fi, :, :] = np.load(
                        self.export_dir + '_thread_' + str(thread_id) +
                        '_interpolated/f_' + str(fi) + '_T_rx_path_' +
                        str(pi) + '.npy')

            if 'Zxx' in self.cmps[pi]:
                fields = np.append(fields, Z[:, :, 0, 0].ravel())
            if 'Zxy' in self.cmps[pi]:
                fields = np.append(fields, Z[:, :, 0, 1].ravel())
            if 'Zyx' in self.cmps[pi]:
                fields = np.append(fields, Z[:, :, 1, 0].ravel())
            if 'Zyy' in self.cmps[pi]:
                fields = np.append(fields, Z[:, :, 1, 1].ravel())
            if 'Tx' in self.cmps[pi]:
                fields = np.append(fields, T[:, :, 0].ravel())
            if 'Ty' in self.cmps[pi]:
                fields = np.append(fields, T[:, :, 1].ravel())

        if self.data_mask is not None:
            fields =  fields[self.data_mask]
        self.last_response = np.hstack((fields.real, fields.imag))

    def import_jacobian(self):

        J_t = np.array([], dtype=complex).reshape(self.n_res, 0)

        if self.inv_type == 'cells':
            J_import = [[] for i in range(self.n_freqs)]
        elif self.inv_type == 'domains':
            J_import = []

        thread_id = 0
        for fi in range(self.n_freqs):
            if self.inv_type == 'cells':
                for ni in range(self.n_procs):
                    try:
                        sub_jacobi = self.import_sub_jacobian(fi, thread_id, ni)
                    except FileNotFoundError:
                        thread_id += 1
                        sub_jacobi = self.import_sub_jacobian(fi, thread_id, ni)
                    J_import[fi].append([sub_jacobi[k] for k in sub_jacobi])

            elif self.inv_type == 'domains':
                try:
                    sub_jacobi = self.import_sub_jacobian(fi, thread_id)
                except FileNotFoundError:
                    thread_id += 1
                    sub_jacobi = self.import_sub_jacobian(fi, thread_id)
                J_import.append([sub_jacobi[k] for k in sub_jacobi])

        for pi in range(self.n_rx):
            if self.config['approach'] == 'E_t':
                jacobi = np.zeros((self.n_res, len(self.tx_ids[pi]),
                                   self.n_freqs, len(self.rxs[pi]),
                                   len(self.cmps[pi])), dtype=complex)

                for fi in range(self.n_freqs):
                    if self.inv_type == 'cells':
                        for ni in range(self.n_procs):
                            if len(J_import[fi][ni][-1]) > 0:
                                jacobi[J_import[fi][ni][-1], :, fi, :, :] =\
                                    J_import[fi][ni][pi]
                    elif self.inv_type == 'domains':
                        jacobi[:, :, fi, :, :] = J_import[fi][pi]

                for ti in range(len(self.tx_ids[pi])):
                    a = 0
                    if not self.magarrow:
                        if 'Ex' in self.cmps[pi]:
                            J_t = self.stack_jacobian(J_t, jacobi[:, ti, :, :, a])
                            a += 1
                        if 'Ey' in self.cmps[pi]:
                            J_t = self.stack_jacobian(J_t, jacobi[:, ti, :, :, a])
                            a += 1
                        if 'Ez' in self.cmps[pi]:
                            J_t = self.stack_jacobian(J_t, jacobi[:, ti, :, :, a])
                            a += 1
                        if any(m in self.cmps[pi] for m in ('Hx', 'Bx')):
                            if self.b_fields:
                                jacobi[:, ti, :, :, a] *= self.mu_nt
                            J_t = self.stack_jacobian(J_t, jacobi[:, ti, :, :, a])
                            a += 1
                        if any(m in self.cmps[pi] for m in ('Hy', 'By')):
                            if self.b_fields:
                                jacobi[:, ti, :, :, a] *= self.mu_nt
                            J_t = self.stack_jacobian(J_t, jacobi[:, ti, :, :, a])
                            a += 1
                        if any(m in self.cmps[pi] for m in ('Hz', 'Bz')):
                            if self.b_fields:
                                jacobi[:, ti, :, :, a] *= self.mu_nt
                            J_t = self.stack_jacobian(J_t, jacobi[:, ti, :, :, a])
                    else:
                        jacobi[:, ti, :, :, :] *= self.mu_nt
                        new_jacobi = np.zeros_like(jacobi[:, ti, :, :, 0])

                        scale = np.linalg.norm([self.east, self.north, self.up])
                        for i in range(len(jacobi)):
                            NEDdata = np.zeros((3, len(jacobi[i, ti, :, :, 0].ravel())),
                                               dtype=complex)
                            NEDdata[0] = jacobi[i, ti, :, :, 0].ravel()
                            NEDdata[1] = jacobi[i, ti, :, :, 1].ravel()
                            NEDdata[2] = jacobi[i, ti, :, :, 2].ravel()
                            tmp_real = np.dot(NEDdata.real.T,
                                              [self.east, self.north, self.up]) / scale
                            tmp_imag = np.dot(NEDdata.imag.T,
                                              [self.east, self.north, self.up]) / scale
                            new_jacobi[i, :, :] = tmp_real.reshape(self.n_freqs, -1) +\
                            1j * tmp_imag.reshape(self.n_freqs, -1)

                        J_t = self.stack_jacobian(J_t, new_jacobi)

            elif self.config['approach'] == 'MT':
                jacobi = np.zeros((self.n_res,
                                   self.n_freqs, len(self.rxs[pi]),
                                   len(self.cmps[pi])), dtype=complex)

                for fi in range(self.n_freqs):
                    if self.inv_type == 'cells':
                        for ni in range(self.n_procs):
                            if len(J_import[fi][ni][-1]) > 0:
                                jacobi[J_import[fi][ni][-1], fi, :, :] =\
                                    J_import[fi][ni][pi]
                    elif self.inv_type == 'domains':
                        jacobi[:, fi, :, :] = J_import[fi][pi]

                a = 0
                if 'Zxx' in self.cmps[pi]:
                    J_t = self.stack_jacobian(J_t, jacobi[:, :, :, a])
                    a += 1
                if 'Zxy' in self.cmps[pi]:
                    J_t = self.stack_jacobian(J_t, jacobi[:, :, :, a])
                    a += 1
                if 'Zyx' in self.cmps[pi]:
                    J_t = self.stack_jacobian(J_t, jacobi[:, :, :, a])
                    a += 1
                if 'Zyy' in self.cmps[pi]:
                    J_t = self.stack_jacobian(J_t, jacobi[:, :, :, a])
                    a += 1
                if 'Tx' in self.cmps[pi] :
                    J_t = self.stack_jacobian(J_t, jacobi[:, :, :, a])
                    a += 1
                if 'Ty' in self.cmps[pi]:
                    J_t = self.stack_jacobian(J_t, jacobi[:, :, :, a])

        self.J = J_t.T
        if self.data_mask is not None:
            self.J =  self.J[self.data_mask]
        self.J = np.vstack((self.J.real, self.J.imag))
        # np.save(self.inv_dir + 'J0.npy', self.J)
        # raise SystemExit

    def import_sub_jacobian(self, fi, idx, ni=None):

        self.import_name = self.export_dir + '_thread_' + str(idx) +\
                            '_inv/f_' + str(fi) + '_J'

        if self.config['approach'] == 'MT':
            self.import_name += '_MT'
        if ni is None:
            self.import_name += '.npz'
        else:
            self.import_name += '_proc_' + str(ni) + '.npz'
        return(np.load(self.import_name))

    def stack_jacobian(self, J_t, sub_jacobi):

        return(np.hstack((J_t, sub_jacobi.reshape(self.n_res, -1))))

    def analyze(self, iteration, inv):

        self.iters.append('iteration ' + str(self.iteration) + ' :')
        self.chi2.append(inv.chi2())
        np.savetxt(self.inv_dir + 'chi2.dat',
                   np.c_[self.iters, self.chi2], fmt='%14s %10s')
        np.save(self.inv_dir + 'sig_iter_' + str(self.iteration) + '.npy',
                inv.model)
        np.save(self.inv_dir + 'response_iter_' + str(self.iteration) +
                '.npy', self.last_response)

        inner = self.mesh()
        inner['res'] = 1./inv.model
        inner.exportVTK(self.inv_dir + 'latest_invmodel.vtk')

    def export_npz(self, fname, data, abs_error, thres=1e-12):

        d_real, d_imag = np.split(data, 2)
        err_real, err_imag = np.split(abs_error, 2)

        # deactivate noisy data
        d_complex = d_real + 1j * d_imag
        # d_complex[np.abs(d_complex.real) < thres] = np.nan + 1j * np.nan
        # d_complex[np.abs(d_complex.imag) < thres] = np.nan + 1j * np.nan

        # multiple Tx export for calculations
        DATA0, lines = [], []
        ni = 0
        for ti, ids in enumerate(self.tx_ids):
            nrx = len(self.rxs[ti])
            npi = len(self.cmps[ti]) * self.n_freqs * nrx
            ntx = len(ids)
            if self.config['approach'] == 'MT':
                ntx = 1
            size = (ntx, len(self.cmps[ti]), self.n_freqs, nrx)
            size2 = (len(self.cmps[ti]), self.n_freqs, nrx)
            darray = np.zeros(size, dtype=complex)
            earray = np.zeros(size, dtype=complex)
            for i in range(len(ids)):
                darray[i, :, :, :] = d_complex[ni:ni+npi].real.reshape(
                    size2) + 1j * d_complex[ni:ni+npi].imag.reshape(size2)
                earray[i, :, :, :] = err_real[ni:ni+npi].reshape(
                    size2) + 1j * err_imag[ni:ni+npi].reshape(size2)
                ni += npi
                if self.config['approach'] == 'MT':
                    break

            data = dict(dataR=darray.real,
                        dataI=darray.imag,
                        errorR=earray.real,
                        errorI=earray.imag,
                        rx=self.rxs[ti], cmp=self.cmps[ti],
                        tx_ids=self.tx_ids[ti], line=np.ones(nrx*len(ids)))
            DATA0.append(data)
            lines.extend(np.ones(npi))

        if self.config['approach'] == 'MT':
            fname0 = fname
        else:
            fname0 = fname + 'ToImport'

        np.savez(fname0 + ".npz",
                 tx=self.txs,
                 freqs=self.freqs,
                 cmp=[1, 1, 1],
                 DATA=DATA0,
                 line=None,
                 origin=[0., 0., 0.],
                 rotation=0.)

        # single Tx export for plotting

        if self.config['approach'] != 'MT':
            txs_npz, rxs_npz, cmps, tx_ids = [], [], [], []
            for ti, ids in enumerate(self.tx_ids):
                for idx in ids:
                    txs_npz.append(np.array(self.txs[int(idx)]))
                    tx_ids.append([int(idx)])
                    rxs_npz.append(np.array(self.rxs[ti]))
                    cmps.append(self.cmps[ti])

            ni = 0
            DATA, lines = [], []
            for pi in range(len(txs_npz)):
                nrx = len(rxs_npz[pi])
                npi = len(cmps[pi]) * self.n_freqs * nrx
                size = (1, len(cmps[pi]), self.n_freqs, nrx)
                data = dict(dataR=d_complex[ni:ni+npi].real.reshape(size),
                            dataI=d_complex[ni:ni+npi].imag.reshape(size),
                            errorR=err_real[ni:ni+npi].reshape(size),
                            errorI=err_imag[ni:ni+npi].reshape(size),
                            rx=rxs_npz[pi], cmp=cmps[pi], tx_ids=tx_ids[pi],
                            line=np.ones(nrx))
                DATA.append(data)
                lines.extend(np.ones(npi))
                ni += npi

            np.savez(fname + 'ToPlot' + ".npz",
                     tx=txs_npz,
                     freqs=self.freqs,
                     cmp=[1, 1, 1],
                     DATA=DATA,
                     line=lines,
                     origin=[0., 0., 0.],
                     rotation=0.)

    # def export_npz(self, fname, data, abs_error, thres=1e-12):

    #     d_real, d_imag = np.split(data, 2)
    #     err_real, err_imag = np.split(abs_error, 2)

    #     DATA0, lines = [], []
    #     ni = 0
    #     for ti, ids in enumerate(self.tx_ids):
    #         nrx = len(self.rxs[ti])
    #         npi = len(self.cmps[ti]) * self.n_freqs * nrx
    #         ntx = len(ids)
    #         if self.config['approach'] == 'MT':
    #             ntx = 1
    #         size = (ntx, len(self.cmps[ti]), self.n_freqs, nrx)
    #         size2 = (len(self.cmps[ti]), self.n_freqs, nrx)
    #         darray = np.zeros(size, dtype=complex)
    #         earray = np.zeros(size, dtype=complex)
    #         for i in range(len(ids)):
    #             darray[i, :, :, :] = d_real[ni:ni+npi].reshape(
    #                 size2) + 1j * d_imag[ni:ni+npi].reshape(size2)
    #             earray[i, :, :, :] = err_real[ni:ni+npi].reshape(
    #                 size2) + 1j * err_imag[ni:ni+npi].reshape(size2)
    #             ni += npi
    #             if self.config['approach'] == 'MT':
    #                 break

    #         data = dict(dataR=darray.real,
    #                     dataI=darray.imag,
    #                     errorR=earray.real,
    #                     errorI=earray.imag,
    #                     rx=self.rxs[ti], cmp=self.cmps[ti],
    #                     tx_ids=self.tx_ids[ti], line=np.ones(nrx*len(ids)))
    #         DATA0.append(data)
    #         lines.extend(np.ones(npi))

    #     n_cmps = max([len(cmp) for cmp in self.cmps])
    #     np.savez(fname+".npz",
    #              tx=self.txs,
    #              freqs=self.freqs,
    #              cmp=[1]*n_cmps,
    #              DATA=DATA0,
    #              line=None,
    #              origin=[0., 0., 0.],
    #              rotation=0.)

    def generate_jacobi_mask(self, freqs, cmps, data, approach):

        real = np.swapaxes(np.isfinite(data["dataR"]), 1, 3)
        imag = np.swapaxes(np.isfinite(data["dataI"]), 1, 3)
        if approach == 'MT':
            flat_cmp = ''.join([item for sublist in cmps for item in sublist])
            if 'Z' in flat_cmp and 'T' in flat_cmp:
                l_cmp = 5
            elif 'Z' in flat_cmp:
                l_cmp = 4
            else:
                l_cmp = 3

        for fi in range(len(freqs)):
            if real[:, :, fi, :].shape != imag[:, :, fi, :].shape:
                print('Real and Imag masks must be identical. '
                      'Aborting  ...')
                raise SystemExit

            if approach == 'E_t':
                self.jacobi_mask[fi].append(real[:, :, fi, :])
            elif approach == 'MT':
                self.jacobi_mask[fi].append(np.repeat(np.repeat(
                        np.logical_or.reduce((
                            np.rollaxis(real[:, :, fi, :2], 2))),
                        2, axis=0)[:, :, np.newaxis], l_cmp, axis=2))



    def coverage(inv, invmodel):
        """ Calculate coverage of inversion results. """
    
        if hasattr(inv.fop, '_jac'):
            # for single forward operator
            cov = np.zeros(inv.fop._jac.cols())
    
            dataScale = inv.dataTrans.deriv(inv.fop.last_response) / \
                inv.dataTrans.error(inv.fop.last_response, inv.fop.errors)
            for i in range(inv.fop._jac.rows()):
                cov += np.abs(inv.fop._jac.row(i) * dataScale[i])
            cov /= inv.modelTrans.deriv(invmodel) # previous * invmodel
            cov /= inv.fop.mesh().cellSizes()
        else:
            # for joint modelling
            cov = np.zeros(inv.fop.fops[0]._jac.cols())
            for n in range(len(inv.fop.fops)):
                dataScale = inv.dataTrans.deriv(inv.fop.fops[n].last_response) / \
                    inv.dataTrans.error(inv.fop.fops[n].last_response,
                                        inv.fop.fops[n].errors)
                for i in range(inv.fop.fops[n]._jac.rows()):
                    cov += np.abs(inv.fop.fops[n]._jac.row(i) * dataScale[i])
            cov /= inv.modelTrans.deriv(invmodel) # previous * invmodel
            cov /= inv.fop.fops[0].mesh().cellSizes()
    
        return cov

