# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import dolfin as df
import numpy as np
from scipy.io import savemat


class MTJacobi:

    def __init__(self, PP, J_eh, cids, fi, n_res, cmp, remote_station):

        """
        Postprocessing of Jacobis for MT
        """

        self.PP = PP
        self.J_eh = J_eh
        self.rm_stn = remote_station
        self.import_derivatives(fi, n_res)
        self.sum_up_ned(fi, cids, cmp, n_res)
        

    def import_derivatives(self, fi, n_res):

        self.dZE, self.dZH, self.dTH = [], [], []
        for pi in range(self.PP.MP.n_rx):
            dZE = np.load(self.PP.export_dir + '_interpolated/f_' +
                          str(fi) + '_dZE_rx_path_' + str(pi) + '.npy')
            self.dZE.append(np.tile(np.swapaxes(dZE,0,1),
                                    (n_res, 1, 1, 1, 1,1)))
            dZH = np.load(self.PP.export_dir + '_interpolated/f_' +
                          str(fi) + '_dZH_rx_path_' + str(pi) + '.npy')
            self.dZH.append(np.tile(np.swapaxes(dZH,0,1),
                                    (n_res, 1, 1, 1, 1,1)))
            dTH = np.load(self.PP.export_dir + '_interpolated/f_' +
                          str(fi) + '_dTH_rx_path_' + str(pi) + '.npy')
            self.dTH.append(np.tile(np.swapaxes(dTH,0,1),
                                    (n_res, 1, 1, 1, 1)))

    def sum_up_ned(self, fi, cids, cmp, n_res):

        self.dZT = []
        for pi in range(self.PP.MP.n_rx):
            ni = 0
            self.dZT.append(np.zeros((len(cids),
                                      len(self.PP.MP.rx[pi]),
                                      len(cmp[pi])),
                                      dtype=complex))
            if 'Zxx' in cmp[pi]:
                self.dZT[-1][:, :, ni] = \
                    np.sum(self.dZE[pi][cids, :, :, 0, 0, 0] *
                           self.J_eh[pi][:, :, :, 1], 1) + \
                    np.sum(self.dZH[pi][cids, :, :, 0, 0, 0] *
                           self.J_eh[pi][:, :, :, 3], 1) + \
                    np.sum(self.dZH[pi][cids, :, :, 1, 0, 0] *
                           self.J_eh[pi][:, :, :, 2], 1)
                ni += 1

            if 'Zxy' in cmp[pi]:
                self.dZT[-1][:, :, ni] = \
                    np.sum(self.dZE[pi][cids, :, :, 0, 0, 1] *
                           self.J_eh[pi][:, :, :, 1], 1) + \
                    np.sum(self.dZH[pi][cids, :, :, 0, 0, 1] *
                           self.J_eh[pi][:, :, :, 3], 1) + \
                    np.sum(self.dZH[pi][cids, :, :, 1, 0, 1] *
                           self.J_eh[pi][:, :, :, 2], 1)
                ni += 1

            if 'Zyx' in cmp[pi]:
                self.dZT[-1][:, :, ni] = \
                    np.sum(self.dZE[pi][cids, :, :, 1, 1, 0] *
                           self.J_eh[pi][:, :, :, 0], 1) + \
                    np.sum(self.dZH[pi][cids, :, :, 0, 1, 0] *
                           self.J_eh[pi][:, :, :, 3], 1) + \
                    np.sum(self.dZH[pi][cids, :, :, 1, 1, 0] *
                           self.J_eh[pi][:, :, :, 2], 1)
                ni += 1

            if 'Zyy' in cmp[pi]:
                self.dZT[-1][:, :, ni] = \
                    np.sum(self.dZE[pi][cids, :, :, 1, 1, 1] *
                           self.J_eh[pi][:, :, :, 0], 1) + \
                    np.sum(self.dZH[pi][cids, :, :, 0, 1, 1] *
                           self.J_eh[pi][:, :, :, 3], 1) + \
                    np.sum(self.dZH[pi][cids, :, :, 1, 1, 1] *
                           self.J_eh[pi][:, :, :, 2], 1)
                ni += 1

            ti = 0
            if any(val in cmp[pi] for val in ['Zxx', 'Zxy', 'Zyx', 'Zyy']):
                ti += 2

            if self.rm_stn is None:
                if 'Tx' in cmp[pi]:
                    self.dZT[-1][:, :, ni] = \
                        np.sum(self.dTH[pi][cids, :, :, 0, 0] *
                               self.J_eh[pi][:, :, :, ti + 1], 1) + \
                        np.sum(self.dTH[pi][cids, :, :, 1, 0] *
                               self.J_eh[pi][:, :, :, ti + 0], 1) - \
                        np.sum(self.dTH[pi][cids, :, :, 2, 0] *
                               self.J_eh[pi][:, :, :, ti + 2], 1)
                    ni += 1

                if 'Ty' in cmp[pi]:
                    self.dZT[-1][:, :, ni] = \
                        np.sum(self.dTH[pi][cids, :, :, 0, 1] *
                               self.J_eh[pi][:, :, :, ti + 1], 1) + \
                        np.sum(self.dTH[pi][cids, :, :, 1, 1] *
                               self.J_eh[pi][:, :, :, ti + 0], 1) - \
                        np.sum(self.dTH[pi][cids, :, :, 2, 1] *
                               self.J_eh[pi][:, :, :, ti + 2], 1)
            else:
                print('remote_jacobi')
                if 'Tx' in cmp[pi]:
                    self.dZT[-1][:, :, ni] = \
                        np.sum(self.dTH[pi][cids, :, :, 0, 0] *
                               self.J_eh[pi][:, :, [-1], ti + 1], 1) + \
                        np.sum(self.dTH[pi][cids, :, :, 1, 0] *
                               self.J_eh[pi][:, :, [-1], ti + 0], 1) - \
                        np.sum(np.expand_dims(self.dTH[pi][cids, :, [-1], 2, 0],2) *
                               self.J_eh[pi][:, :, :, ti + 2], 1)
                    ni += 1

                if 'Ty' in cmp[pi]:
                    self.dZT[-1][:, :, ni] = \
                        np.sum(self.dTH[pi][cids, :, :, 0, 1] *
                               self.J_eh[pi][:, :, [-1], ti + 1], 1) + \
                        np.sum(self.dTH[pi][cids, :, :, 1, 1] *
                               self.J_eh[pi][:, :, [-1], ti + 0], 1) - \
                        np.sum(np.expand_dims(self.dTH[pi][cids, :, [-1], 2, 1],2) *
                               self.J_eh[pi][:, :, :, ti + 2], 1)


        # mat_dict = dict()
        # mat_dict["J_eh"] = self.J_eh
        # mat_dict["dTH"] = self.dTH
        # mat_dict["dZT"] = self.dZT
        # savemat(self.PP.export_dir + '_inv/f_' + str(fi) +
        #         '_J_MT_proc_' + str(df.MPI.rank(df.MPI.comm_world)) +
        #         '.mat', mat_dict)

        if len(cids) == n_res:
            if df.MPI.rank(self.PP.MP.mpi_cw) == 0:
                np.savez(self.PP.export_dir + '_inv/f_' + str(fi) +
                          '_J_MT.npz', *self.dZT)
        else:
            np.savez(self.PP.export_dir + '_inv/f_' + str(fi) +
                      '_J_MT_proc_' + str(df.MPI.rank(df.MPI.comm_world)) +
                      '.npz', *self.dZT, cids)

    # def sum_up_enu(self, fi, cids, cmp):


    #     self.dZT = []
    #     for pi in range(self.PP.MP.n_rx):
    #         ni = 0
    #         self.dZT.append(np.zeros((len(cids),
    #                                   len(self.PP.MP.rx[pi]),
    #                                   len(cmp[pi])),
    #                                   dtype=complex))
    #         if 'Zxx' in cmp[pi]:
    #             self.dZT[-1][:, :, ni] = np.sum(self.dZE[pi][cids, :, :, 0, 0, 0] *
    #                                            self.J_eh[pi][:, :, :, 0], 1) +\
    #                                      np.sum(self.dZH[pi][cids, :, :, 0, 0, 0] *
    #                                            self.J_eh[pi][:, :, :, 2], 1) +\
    #                                      np.sum(self.dZH[pi][cids, :, :, 1, 0, 0] *
    #                                             self.J_eh[pi][:, :, :, 3], 1)
    #             ni += 1

    #         if 'Zxy' in cmp[pi]:
    #             self.dZT[-1][:, :, ni] = np.sum(self.dZE[pi][cids, :, :, 0, 0, 1] *
    #                                             self.J_eh[pi][:, :, :, 0], 1) +\
    #                                      np.sum(self.dZH[pi][cids, :, :, 0, 0, 1] *
    #                                             self.J_eh[pi][:, :, :, 2], 1) +\
    #                                      np.sum(self.dZH[pi][cids, :, :, 1, 0, 1] *
    #                                             self.J_eh[pi][:, :, :, 3], 1)
    #             ni += 1

    #         if 'Zyx' in cmp[pi]:
    #             self.dZT[-1][:, :, ni] = np.sum(self.dZE[pi][cids, :, :, 1, 1, 0] *
    #                                             self.J_eh[pi][:, :, :, 1], 1) +\
    #                                      np.sum(self.dZH[pi][cids, :, :, 0, 1, 0] *
    #                                             self.J_eh[pi][:, :, :, 2], 1) +\
    #                                      np.sum(self.dZH[pi][cids, :, :, 1, 1, 0] *
    #                                             self.J_eh[pi][:, :, :, 3], 1)
    #             ni += 1

    #         if 'Zyy' in cmp[pi]:
    #             self.dZT[-1][:, :, ni] = np.sum(self.dZE[pi][cids, :, :, 1, 1, 1] *
    #                                             self.J_eh[pi][:, :, :, 1], 1) +\
    #                                      np.sum(self.dZH[pi][cids, :, :, 0, 1, 1] *
    #                                             self.J_eh[pi][:, :, :, 2], 1) +\
    #                                      np.sum(self.dZH[pi][cids, :, :, 1, 1, 1] *
    #                                             self.J_eh[pi][:, :, :, 3], 1)
    #             ni += 1

    #         if 'Tx' in cmp[pi]:
    #             self.dZT[-1][:, :, ni] = np.sum(self.dTH[pi][cids, :, :, 0, 0] *
    #                                             self.J_eh[pi][:, :, :, 0], 1) +\
    #                                      np.sum(self.dTH[pi][cids, :, :, 1, 0] *
    #                                             self.J_eh[pi][:, :, :, 1], 1) +\
    #                                      np.sum(self.dTH[pi][cids, :, :, 2, 0] *
    #                                             self.J_eh[pi][:, :, :, 2], 1)
    #             ni += 1

    #         if 'Ty' in cmp[pi]:
    #             self.dZT[-1][:, :, ni] = np.sum(self.dTH[pi][cids, :, :, 0, 1] *
    #                                             self.J_eh[pi][:, :, :, 0], 1) +\
    #                                      np.sum(self.dTH[pi][cids, :, :, 1, 1] *
    #                                             self.J_eh[pi][:, :, :, 1], 1) +\
    #                                      np.sum(self.dTH[pi][cids, :, :, 2, 1] *
    #                                             self.J_eh[pi][:, :, :, 2], 1)

    #     np.savez(self.PP.export_dir + '_inv/f_' + str(fi) +
    #               '_J_MT_proc_' + str(df.MPI.rank(df.MPI.comm_world)) +
    #               '.npz', *self.dZT, cids)