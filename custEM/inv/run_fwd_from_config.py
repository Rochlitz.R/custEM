# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

from custEM.core import MOD
# from custEM.misc import mpi_print as mpp
import sys
import json
import dolfin as df
import numpy as np
import os

config = json.load(open(sys.argv[1]))
thread_id = int(sys.argv[2])

M = MOD(config['mod'] + '_thread_' + str(thread_id),
        config['mesh'], config['approach'],
        p=config['p_fwd'], jacobian=config['jacobian'],
        debug_level=50, serial_ordering=True, export_domains=False,
        overwrite_results=True, overwrite_mesh=True,solvingapproach=[config['solvingapproach0'],
        config['solvingapproach1'],config['solvingapproach2'],config['solvingapproach3']])      

if 'ip_c' in config:
    M.MP.update_model_parameters(frequencies=config['freqs'],
                                 sigma_ground=config['sig'],
                                 ip_c=config['ip_c'],
                                 ip_m=config['ip_m'],
                                 ip_tau=config['ip_tau'])
else:
    M.MP.update_model_parameters(frequencies=config['freqs'],
                                 sigma_ground=config['sig'])

if config['jacobian']:
    M.INV.update_inv_parameters(iteration=config['iteration'],
                                components=config['components'],
                                tx_ids=config['tx_ids'],
                                skip_domains=config['skip_domains'])

if 'ip_c' in config:
    M.FE.build_var_form(ip=True)
else:
    M.FE.build_var_form()

if config['jacobian']:
    #if df.MPI.comm_world.rank == 0:
    #      freq=config['freqs']
    #      id=int(thread_id)
    #      freq=freq[id]
    #      PID = os.getpid()
    #      with open('outerrelres' + str(PID) +'.txt','ab') as fil:
    #        np.savetxt(fil, [], header='Frequency='+str(freq), comments='# ',  fmt='%1.4e')
    M.solve_main_problem(
        convert=False,
        auto_interpolate=False,
        calculation_frequencies=config['calc_freqs'][thread_id],
        inv_dir=config['inv_dir'],
        remote_station=config['remote_station'])
else:
    M.solve_main_problem(
        auto_interpolate=True,
        export_pvd=config['save_pvd'],
        export_nedelec=config['save_pvd'],
        export_cg=False,
        calculation_frequencies=config['calc_freqs'][thread_id])
    if config['approach'] == 'MT':
        M.IB.convert_mt_data(
            ned_coord_system=True,
            derivatives=True,
            freqs=config['calc_freqs'][thread_id],
            remote_station=config['remote_station'])


if config['save_pvd']:
    interp_meshes = []
    for ri, path in enumerate(M.MP.rx):
        M.IB.create_path_mesh(path, path_name=str(ri) + str(thread_id))
        interp_meshes.append(str(ri) + str(thread_id) +'_path')

    M.IB.interpolate(interp_meshes, ['E_t', 'H_t'],
                     freqs=config['calc_freqs'][thread_id])
