# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

import shutil


try:
    shutil.rmtree('base_test_results')
except:
    pass

try:
    shutil.rmtree('fd_test_results')
except:
    pass

try:
    shutil.rmtree('td_test_results')
except:
    pass

try:
    shutil.rmtree('mesh_test_results')
except:
    pass

try:
    shutil.rmtree('topo_test_results')
except:
    pass

try:
    shutil.rmtree('meshes')
except:
    pass

try:
    shutil.rmtree('results')
except:
    pass

try:
    shutil.rmtree('fd_test_plots')
except:
    pass

try:
    shutil.rmtree('td_test_plots')
except:
    pass

try:
    shutil.rmtree('field_empymod.vtk')
except:
    pass

try:
    shutil.rmtree('field_pyhed.vtk')
except:
    pass
