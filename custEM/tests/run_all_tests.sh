#!/bin/bash

echo "Running basic custEM tests ..."
export OMP_NUM_THREADS=1
log_file=/home61/rochlitz/conda_build/test.log
echo $log_file
python clean_up.py
echo "Log file initialized" > $log_file
mpirun -n 4 python -u test_basics.py >> $log_file & PID0=$!
wait $PID0
mpirun -n 2 python test_mesh_generation.py >> $log_file & PID1=$!
wait $PID1
mpirun -n 10 python -u test_fd_approaches.py >> $log_file & PID2=$!
wait $PID2
mpirun -n 6 python -u test_td_approaches.py >> $log_file & PID3=$!
wait $PID3
mpirun -n 32 python -u test_topo_model.py >> $log_file & PID4=$!
wait $PID4

#echo "Running interpolation and plot tests ..."
#mpirun -n 48 python -u interpolate_fd_test_results.py >> $log_file & PID4=$!
#wait $PID4
#python plot_fd_test_results.py >> $log_file & PID5=$!
#wait $PID5

python clean_up.py