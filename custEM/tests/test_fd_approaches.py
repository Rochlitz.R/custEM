# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

from custEM.core import MOD
from custEM.misc import max_mem
from custEM.misc import run_serial
import dolfin as df
import numpy as np
import os
import shutil


"""
Create simple meshes for testing computations with all
frequency-domain approached supported by custEM and if
the *pyGIMLi* and TetGen libraries are installed properly.

Uncomment the **df.set_log_level(df.DEBUG)** command to see what FEniCS prints.
"""

mpi_cw = df.MPI.comm_world
mpi_size = df.MPI.size(mpi_cw)

if df.MPI.rank(df.MPI.comm_world) == 0:
    try:
        shutil.rmtree('fd_test_results')
    except FileNotFoundError:
        pass
else:
    pass

df.MPI.barrier(mpi_cw)

# %% Create Mesh

if not os.path.isfile('meshes/_h5/hed_halfspace_mesh.h5'):
    run_serial('create_test_meshes_from_mpi.py')

# %% Testing HED halfspace model with total and secondary E-field approach

M = MOD('fd_test_E_t', 'hed_halfspace_mesh', 'E_t', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='fd_test_results',
        para_dir='meshes/para')

M.MP.update_model_parameters(omega=1e2 * 2. * np.pi)

M.FE.build_var_form(s_type='hed',
                    length=0.2)

M.solve_main_problem(export_cg=True)

###############################################################################

M = MOD('fd_test_E_s_pf_E', 'hed_halfspace_mesh', 'E_s', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='fd_test_results',
        para_dir='meshes/para')

M.MP.update_model_parameters(omega=1e2 * 2. * np.pi,
                             sigma_ground=1e-2,
                             sigma_air=1e-6)

M.FE.build_var_form(s_type='hed',
                    n_segs=10,
                    pf_EH_flag='E')

M.solve_main_problem(export_cg=True)

###############################################################################

M = MOD('fd_test_E_s_pf_H', 'hed_halfspace_mesh', 'E_s', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='fd_test_results',
        para_dir='meshes/para')

M.MP.update_model_parameters(omega=1e2 * 2. * np.pi)

M.FE.build_var_form(s_type='hed',
                    n_segs=10,
                    pf_EH_flag='H')

M.solve_main_problem(export_cg=True)

###############################################################################

if mpi_size > 1:
    if mpi_cw.Get_rank() == 0:
        print('\n')
        print('##############################################################')
        print('    HED halfspace test performed succesfully in mpi mode !!!  ')
        print('    tested *E_t*, *E_s* and *H_s* approaches with E and H pf  ')
        print('##############################################################')
        print('\n')
    else:
        pass
else:
    print('\n')
    print('##############################################################')
    print('          HED halfspace test performed succesfully !!!        ')
    print('    tested *E_t*, *E_s* and *H_s* approaches with E and H pf  ')
    print('##############################################################')
    print('\n')

max_mem()


###############################################################################

# %% Testing Line source halfspace model with mixed potential approaches

M = MOD('fd_test_Am_t', 'line_dip_plate_mesh', 'Am_t', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='fd_test_results',
        para_dir='meshes/para')

M.MP.update_model_parameters(omega=1e1 * 2. * np.pi,
                             sigma_ground=[1e-1, 1e-2, 1e-1],
                             sigma_0=[1e-1, 1e-2, 1e-2],
                             sigma_air=1e-6)

M.FE.build_var_form(s_type='line',
                    start=[-500.0, 0., 0.],
                    stop=[500.0, 0., 0.])

M.solve_main_problem(export_cg=True)

###############################################################################

M = MOD('fd_test_An_t', 'line_dip_plate_mesh', 'An_t', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='fd_test_results',
        para_dir='meshes/para')

M.MP.update_model_parameters(omega=1e1 * 2. * np.pi,
                             sigma_ground=[1e-1, 1e-2, 1e-1],
                             sigma_0=[1e-1, 1e-2, 1e-2],
                             sigma_air=1e-6)

M.FE.build_var_form(s_type='line',
                    start=[-500.0, 0., 0.],
                    stop=[500.0, 0., 0.])

M.solve_main_problem(export_cg=True)

###############################################################################

M = MOD('fd_test_Am_s', 'line_dip_plate_mesh', 'Am_s', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='fd_test_results',
        para_dir='meshes/para')

M.MP.update_model_parameters(omega=1e1 * 2. * np.pi,
                             sigma_ground=[1e-1, 1e-2, 1e-1],
                             sigma_0=[1e-1, 1e-2, 1e-2],
                             sigma_air=1e-6)

M.FE.build_var_form(s_type='line',
                    start=[-500.0, 0., 0.],
                    stop=[500.0, 0., 0.],
                    n_segs=50,
                    pf_EH_flag='H')

M.solve_main_problem(export_cg=True)

###############################################################################

M = MOD('fd_test_Fm_s', 'line_dip_plate_mesh', 'Fm_s', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='fd_test_results',
        para_dir='meshes/para')

M.MP.update_model_parameters(omega=1e1 * 2. * np.pi,
                             sigma_ground=[1e-1, 1e-2, 1e-1],
                             sigma_0=[1e-1, 1e-2, 1e-2],
                             sigma_air=1e-6)

M.FE.build_var_form(s_type='line',
                    start=[-500.0, 0., 0.],
                    stop=[500.0, 0., 0.],
                    n_segs=50,
                    pf_EH_flag='E')

M.solve_main_problem(convert_to_E=True, export_cg=True)

###############################################################################

if mpi_size > 1:
    if mpi_cw.Get_rank() == 0:
        print('\n')
        print('##############################################################')
        print('    Line sample test performed succesfully in mpi mode !!!')
        print('    tested *Am_t*, *An_t*, *Am_s* and *Fm_s* approaches   ')
        print('##############################################################')
        print('\n')
    else:
        pass
else:
    print('\n')
    print('##########################################################')
    print('      Line sample test performed succesfully !!!')
    print('    tested *Am_t*, *An_t*, *Am_s* and *Fm_s* approaches   ')
    print('##########################################################')
    print('\n')

max_mem()


###############################################################################

# %% Testing Loop source halfspace model with H and An secondary field appr.

M = MOD('fd_test_H_s_pf_E', 'loop_brick_mesh', 'H_s', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='fd_test_results',
        para_dir='meshes/para')

M.MP.update_model_parameters(omega=2e1 * 2. * np.pi,
                             sigma_ground=[1e-4, 1e-2],
                             sigma_0=[1e-4, 1e-4])

M.FE.build_var_form(s_type='loop_r',
                    start=[-500., -500., 0.],
                    stop=[500., 500., 0.],
                    n_segs=84,
                    pf_EH_flag='E')

M.solve_main_problem(convert_to_E=True, export_cg=True)

###############################################################################

M = MOD('fd_test_H_s_pf_H', 'loop_brick_mesh', 'H_s', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='fd_test_results',
        para_dir='meshes/para')

M.MP.update_model_parameters(omega=2e1 * 2. * np.pi,
                             sigma_ground=[1e-4, 1e-2],
                             sigma_0=[1e-4, 1e-4])

M.FE.build_var_form(s_type='loop_r',
                    start=[-500., -500., 0.],
                    stop=[500., 500., 0.],
                    n_segs=84,
                    pf_EH_flag='H')

M.solve_main_problem(convert_to_E=True, export_cg=True)

###############################################################################

M = MOD('fd_test_An_s', 'loop_brick_mesh', 'An_s', p=1, debug_level=50,
        overwrite=True, m_dir='meshes', r_dir='fd_test_results',
        para_dir='meshes/para')

M.MP.update_model_parameters(omega=2e1 * 2. * np.pi,
                             sigma_ground=[1e-4, 1e-2],
                             sigma_0=[1e-4, 1e-4])

M.FE.build_var_form(s_type='loop_r',
                    start=[-500., -500., 0.],
                    stop=[500., 500., 0.],
                    n_segs=84,
                    pf_EH_flag='E')

M.solve_main_problem(export_cg=True)

if mpi_size > 1:
    if mpi_cw.Get_rank() == 0:
        print('\n')
        print('##############################################################')
        print('      Loop sample test performed succesfully in mpi mode !!!')
        print('      tested *H_s* and *An_s* approaches, H with E or H pf    ')
        print('##############################################################')
        print('\n')
    else:
        pass
else:
    print('\n')
    print('#############################################################')
    print('            Loop sample test performed succesfully !!!       ')
    print('      tested *H_s* and *An_s* approaches, H with E or H pf   ')
    print('#############################################################')
    print('\n')

if mpi_size > 1:
    if mpi_cw.Get_rank() == 0:
        print('\n')
        print('###########################################################')
        print('###########################################################')
        print('###########################################################')
        print('    FD approach test performed successfully in mpi mode!!!  ')
        print('###########################################################')
        print('###########################################################')
        print('###########################################################')
    else:
        pass
else:
    print('\n')
    print('###############################################')
    print('###############################################')
    print('###############################################')
    print('   FD approach test performed successfully!!!   ')
    print('###############################################')
    print('###############################################')
    print('###############################################')
