# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""
import pygimli as pg
import numpy as np
from comet import pyhed as ph
import time

from comet.pyhed.IO import savefieldvtk
from empymod import bipole


def calcLoop_Empymod(loop):

    # magnetic receivers yes/no switch
    mrec = loop.config.ftype.upper() in ['H', 'B']

    # prepare sources
    # number of azimuths not possible: using bipoles
    # (from one dipole to the next, its not exactly the same, but enough for
    # this comparison)
    # (looping over the transmitter from outside empymod is slower)
    # bipoles = [x0, x1, y0, y1, z0, z1]
    dipoles = loop.pos
    dipoles = np.row_stack((dipoles, dipoles[0]))  # add first again for loop
    bipoles = []
    for i in range(len(loop.pos)):  # loop over dipoles
        bipoles.append([dipoles[i, 0], dipoles[i + 1, 0], dipoles[i, 1],
                        dipoles[i + 1, 1], 0.01, 0.01])
    bipoles = np.array(bipoles).T

    # prepare receiver
    receiver = loop.loopmesh.positions().array().T

    # prepare 1D resistivity structure
    depth = [0]
    depth.extend(np.cumsum(loop.config.d).tolist())

    res = [1e20]
    res.extend(loop.config.rho)

    # field
    LoopField = np.zeros((3, loop.loopmesh.nodeCount()), complex)

    # -z to match coordinate system
    # 3 consecutive calls for the three field components (x, y, z)
    kwargs = {'verb': 0, 'mrec': mrec}

    tick_em = time.time()

    recx = [receiver[0], receiver[1], -receiver[2], 0, 0]
    field_hlp_x = bipole(bipoles, recx, depth, res, loop.config.f, **kwargs)

    recy = [receiver[0], receiver[1], -receiver[2], 90, 0]
    field_hlp_y = bipole(bipoles, recy, depth, res, loop.config.f, **kwargs)

    recz = [receiver[0], receiver[1], -receiver[2], 0, -90]
    field_hlp_z = bipole(bipoles, recz, depth, res, loop.config.f, **kwargs)

    ready_em = time.time() - tick_em

    # shape(field_hlp_x/y/z) = (rx, tx)
    # sum up over tx (+ fix nan values previous)
    # hack for vtk export (cant deal with nan for the edges inbetween the
    # bipoles)
    field_hlp_x[np.where(np.isnan(field_hlp_x))] = 0.0
    field_hlp_y[np.where(np.isnan(field_hlp_y))] = 0.0
    field_hlp_z[np.where(np.isnan(field_hlp_z))] = 0.0

    LoopField[0] = np.sum(field_hlp_x, axis=1)
    LoopField[1] = np.sum(field_hlp_y, axis=1)
    LoopField[2] = np.sum(field_hlp_z, axis=1)

    savefieldvtk('field_empymod.vtk', loop.loopmesh, LoopField, verbose=True)
    return ready_em, LoopField


if __name__ == '__main__':
    cfg = ph.config(rho=[1000.],  # 1000 Ohmmeter
                    d=[],         # homogeneous halfspace
                    f=100.,       # frequency [Hz]
                    mode='te',    # closed loop, no grounding
                    ftype='H')    # magnetic field

    loop = ph.loop.buildRectangle([[-500, 500], [500, -500]],
                                  num_segs=20, config=cfg)

    # simple default loop mesh (Nodes: 1846 Cells: 10155) (calc on nodes)
    mesh = pg.load('meshes/_h5/loop_brick_mesh.h5')  # import our testmesh
    loop.setLoopMesh(mesh)

    # loop.show()  # optional: view source discretization
    # print(loop)

    tick_com = time.time()
    loop.calculate(maxCPUCount=1, verbose=True)

    ready_com = time.time() - tick_com

    loop.exportVTK('field_pyhed.vtk')

    # reference calculation with empymod
    ready_em, field_emp = calcLoop_Empymod(loop)

    # runtime: single core (1846 receivers, 20 transmitters, 1 freq)
    # time pyhed: ~ 1 s
    # time empymod: ~ 95 s

    # Additional note: pyhed uses 101 point filter instead of 201 point
    # filter, this reduces the calulation of pyhed compared to empymod by a
    # factor of 2.

    print('time pyhed: {:.2f} s'.format(ready_com))
    print('time empymod: {:.2f} s'.format(ready_em))

# The End
