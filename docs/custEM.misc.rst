custEM.misc package
===================

Submodules
----------

custEM.misc.anomaly\_expressions module
---------------------------------------

.. automodule:: custEM.misc.anomaly_expressions
   :members:
   :undoc-members:
   :show-inheritance:

custEM.misc.misc module
-----------------------

.. automodule:: custEM.misc.misc
   :members:
   :undoc-members:
   :show-inheritance:

custEM.misc.profiler module
---------------------------

.. automodule:: custEM.misc.profiler
   :members:
   :undoc-members:
   :show-inheritance:

custEM.misc.pyhed\_calculations module
--------------------------------------

.. automodule:: custEM.misc.pyhed_calculations
   :members:
   :undoc-members:
   :show-inheritance:

custEM.misc.synthetic\_definitions module
-----------------------------------------

.. automodule:: custEM.misc.synthetic_definitions
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: custEM.misc
   :members:
   :undoc-members:
   :show-inheritance:
