.. _devlabel:

###########
Development
###########

The custEM toolbox is under steady academic development. Aside from a few
exceptions, only one author can spend effort on implementing new features,
updating the documentation, tutorials and examples, or resolving issues.
It is possible that a few examples or methods might not work properly or that
syntax errors occur in different custEM versions, even though we tried to fix
as many issues as possible in custEM version 1.0.0.

We kindly ask for apology in such cases and encourage users to report any bugs,
which is the most effective way to increase the robustness of custEM.
Furthermore, please contact us in case of missing features and we will try to
add the corresponding functionalities.

################################################################################

Release notes
=============

**18.03.2024** - custEM version 1.5.0

* integration of iterative solvers into custEM
* smaller bug fixes for MT/AFMAG inversion

**27.11.2023** - custEM version 1.4.0

* updates for inversion of natural source EM data
* updates for scalar magentometer inversion 
* intermediate merge only, no documentation updates

**15.02.2023** - custEM version 1.3.0

* master branch update including implementation progress in 2022
* improved sensitivity calculations
* improved wrapper for interoperation with pyGIMLi
* updates for handling inversion of natural source data (MT, AFMAG)
* added examples for inversion applications of CSEM data
* moved examples to custEM-examples repository

**01.03.2022** - custEM version 1.2.0

* use superior TetGen version 1.6
* syntax adaption of all existing examples and tutorials
* implemented efficient algebra for Jacobi calculations
* additional practical meshing functionalities to handle real survey data
* many updated on custEM wrapper for pyGIMLi to run inversions
* further updates on MT postprocessing

**30.07.2021** - custEM version 1.1.0

* immense progress on inv_tools
* efficient explicit Jacobi calculations
* efficient threading over multiple frequencies
* wrapper to use custEM as forward operator for pyGIMLi
* updates on MT postprocessing - needs still testing!

**26.04.2021** - custEM version 1.0.0

* final updates for custEM version 1.0
* resolved a few remaining bugs
* overhaul of API doc strings 
* update of all finished examples 
* update of tutorials

**22.04.2021** - custEM version 0.99.95

* pre-release candidate for custEM version 1.0
* resolved a couple of remaining bugs
* resolved issue to convert new meshes with the same name instead of
  accidently using the already converted onces if not deleted manually 
* simplified and updated interpolation mesh generation to work without seperatly
  spawned processes for more simplicity and robustness 
* seperate transmitter currents for multiple Tx in time-domain and DC approaches

**03.03.2021** - custEM version 0.99.94

* manual interpolation matrices implemented 
* automated receiver-position handling
* dg-paramter functions implemented for all approaches as standard
* handling of general anisotropic sigma/mu/eps values now user-friendly
* natural-source modeling approach debugged and post-processing simplified
* added MT Dublin test model and IP examples
* option of serial-ordering for avoiding/reducing? random MUMPS crashes 

**10.02.2021** - custEM version 0.99.93

* major development step towards flexibility, user-friendliness and inversion
* logger from standard Python logging module added and working very well,
  need to replace remaining non-logged prints to write proper log files
* multiple-transmitter support for all time-domain approaches
* electric and magnetic field approaches now support handling multiple
  frequencies internally in combination with multiple transmitters 
* optimized performance exploting reused-factorization as best as possible
* potential approaches can now be updated quite easy to support this feature
  as well, if required by users
* overhaul of many outdated code parts and related name conventions

**28.01.2021** - custEM version 0.99.92 

* includes custEM version 0.99.91
* natural-source modeling approach based on E-field formulation tested
* validation against Dublin test model successful, now proper MT class required
* better handling of arbitrary source currents, source and observation time for
  time-stepping approach added 
* fix of cell-identification bug in RHSAssembly class
* started internal support of multiple frequencies instead of looping over 
  several MOD instances handling only a single frequency 
* further doc-string updates 

**9.12.2020** - custEM version 0.99.90

* removed some minor bugs which still led to crashes in various approaches
* doc strings overhaul and minor optimizations of *mod, fem, misc* sub-modules
* test scripts overhaul and test script for time-domain approaches added
* proper documentaion of time-domain modeling examples
* validation of implementation to consider induced polarization parameters

**19.11.2020** - custEM version 0.99.16

* summarized updates since v. 0.98, see also gitlab descriptions of commits
* main work in this time was spent on applications and time-domain approaches
* advanced mesh tools with node and edge markers
* pyhed fixed, identical sign conversion
* pyhed incorporated as third-party conda package now, not in wrap directory
* time stepping approach optimized
* rational krylov approach added
* inverse fourier transformation based approach added
* documentation updated
* few syntax changes

**12.06.2019** - custEM version 0.98

* conda installer added, installation simplified significantly
* custEM tests during conda package build enabled
* support got FEniCS version older than 2018 disabled
* documentation updated

**20.05.2019** - custEM version 0.97

* alternative tranformation factors for PF E-field tested with help of empymod 
* compatibility with FEniCS 2019.1
* potential approaches now correctly implemented with identical structure
  (based on E=iw*A + iw*grad*V)
  (based on H=iw*F + iw*grad*U)
* corresponding post_processing adapated 
* F-U (Tau-Omega) approach added
* bugfixes in meshgen and bathy tools 

**13.03.2019** - custEM version 0.96

* lot's of bugs fixed (overall compatibility with FEniCS 2018.1)
* multiple RHS supported for total field approaches and framwork for 
  adding this feature for secondary field approaches already implemented
* test, example and tutorial files updated to be again compatible with
  a this new version of custEM
* All provided scripts were tested also with previous versions based on
  FEniCS 2017.2, but it is heavily recommended to use this newest version with
  FEnICS 2018 to benefit from the increased performance!

**09.01.2019** - custEM version 0.95 

* version 0.94 included
* custEM now compatible with FEniCS 2018.1
* Significant performance boost due to parallel HDF5 support for FEniCS conda
  package and new MUMPS version, no matrix size restrictions anymore!
* Performance boost due to debugged symmetric MUMPS solver
  (previous custEM versions were bugged and called the general LU solver)
* Full Maxwell equations with displacement currents for E-field approach
  available, in progress for H-field approach
* H-field approach debugged
* Time-domain time-stepping approach added (not fully supported yet)
* Bathymetry/Topography meshing tools improved
* Irregular topography data supported

**28.08.2018** - custEM version 0.93

* bug for primary E-fields of line sources fixed!
* alternative and superior way of incorporating transmitters in meshes added
* examples 2 reworked
* first steps for meshing costal areas with topography, bathymetry initialized

**28.08.2018** - custEM version 0.92

* ReadtheDocs bug fixed: API for modules added!
* restructured repository (custEM modules now subdirectory of repository)
* examples added to table of contents

**23.08.2018** - custEM version 0.91

* fixed sorting-bug workaround for primary-field calculations!
* removed minor bugs in meshgen submodule
* added topography meshing tutorial to the tutorials directory 
* enabled edge preservation by using not only *.poly* but also *.edge* files for
  TetGen which is especially an advantage for implementing all kind of CSEM
  transmitters. As a consequence, TetGen does not need to be called with the 
  '-M' flag anymore for edge preservation, which saves up to 30% of dofs!

**Summary of main features before 09.08.2018**

* documentation added:

    - documentation of main page, installation notes, etc. added
    - 3 test scripts provided (*tests* directory)
    - 4 examples provided for reproducibility (*examples* directory)
    - 3 tutorials for meshing, FE computation and visualization available
    - automatically updated online documentation on readthedocs.io

* six *approaches* with arbitrary anisotropic conductivities supported for
  finite element modeling of CSEM problems in frequency domain:
  
    - Total and Secondary E-field: *E_t* and *E_s*
    - Secondary H-field: *H_s*
    - Total and Secondary A-Phi on mixed elements: *Am_t* and *Am_s*
    - Total and Secondary A-Phi on nodal elements: *An_s*
      (only xyz-directed anisotropy supported until now, extension possible)
    - automated post-processing
      
* meshgen tools robustly implemented for land-based or airborne CSEM setups:

    - full-space, half-space or layered-earth tetrahedral meshes
    - topography can be applied to any horizontal surface or interface
    - transmitters or observation lines on surface shifted automatically to
      follow topography if required
    - domain markers applied automatically and imported for computation from
      **MOD** instance.
    - arbitrary anomalies within layers or reaching the surface, several
      regularly shaped bodies pre-defined
    - enclosing tetrahedral mesh to increase domain size and minimize boundary
      artifacts

* interpolation on lines or slices:

    - lines or slices parallel to coordinate axes
    - crooked "horizontal" lines or slices following topography
    - multi-threading applied to speed up interpolation
    
* visualization of line or slice data:

    - automated line or slice plots of all electric or magnetic field components
    - misfit calculations and visualization
    - flexible generation of plots with patches combining various data 
