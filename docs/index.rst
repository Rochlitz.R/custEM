.. custEM documentation master file, created by
   sphinx-quickstart on Mon Jun 25 18:14:37 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


##################
Welcome to custEM!
##################

Version: |version| ~ Date: |today|

.. include:: readme.rst

.. toctree::
   :maxdepth: 4
   :hidden:

   install
   modules_static
   tipsandtricks
   development
   tutorials
   examples
   license
   authors
